#include <iostream>
#include <vector>
#include <stack>

using namespace std;

typedef pair<int, int> pii;

const int MAX_N = 500;

int n;
stack<int> s[MAX_N];

vector<pii> path;

int main()
{
    cin >> n;

    int k;
    int x;

    bool stop = true;

    for (int i = 0; i < n;  i ++) {
        cin >> k;
        for (int j = 0; j < k; j ++) {
            cin >> x;
            x --;

            s[i].push(x);

            if (i != x) stop = false;
        }
    }

    if (stop) return 0;

    int a = 0;
    int b = n - 2;
    int c = n - 1;

    if (b < 0) b = 0;
    if (c < 0) c = 0;

    for (int i = 0; i < n; i ++) {
        if (i == a) continue;

        while (!s[i].empty()) {
            s[a].push(s[i].top());
            s[i].pop();
            path.push_back(make_pair(i, a));
        }
    }

    while (!s[a].empty()) {
        x = s[a].top();

        if (x != a) {
            s[x].push(x);
            path.push_back(make_pair(a, x));
        } else {
            s[c].push(x);
            path.push_back(make_pair(a, c));
        }

        s[a].pop();
    }

    while (!s[c].empty()) {
        x = s[c].top();

        if (x != a) {
            s[b].push(x);
            path.push_back(make_pair(c, b));
        } else {
            s[a].push(x);
            path.push_back(make_pair(c, a));
        }

        s[c].pop();
    }

    while (!s[b].empty() && s[b].top() == c) {
        s[c].push(s[b].top());
        path.push_back(make_pair(b, c));
        s[b].pop();
    }

    for (int i = 0; i < n; i ++) {
        while (!s[i].empty()) {
            if (s[i].top() != i) {
                cout << 0 << endl;
                return 0;
            }
            s[i].pop();
        }
    }

    for (int i = 0; i < path.size(); i ++) {
        cout << path[i].first+1 << " " << path[i].second+1 << endl;
    }

    return 0;
}
