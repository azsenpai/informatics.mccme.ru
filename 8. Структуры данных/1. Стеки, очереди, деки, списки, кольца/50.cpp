#include <iostream>
#include <queue>

using namespace std;

queue<int> a;
queue<int> b;

int main()
{
    for (int i = 0, x; i < 5; i ++) {
        cin >> x;
        a.push(x);
    }

    for (int i = 0, x; i < 5; i ++) {
        cin >> x;
        b.push(x);
    }

    int f;
    int s;

    for (int i = 1; i <= 1000000; i ++) {
        f = a.front();
        s = b.front();

        a.pop();
        b.pop();

        if (f == 0 && s == 9) {
            a.push(f);
            a.push(s);
        }
        else if (f == 9 && s == 0) {
            b.push(f);
            b.push(s);
        }
        else if (f > s) {
            a.push(f);
            a.push(s);
        }
        else {
            b.push(f);
            b.push(s);
        }

        if (a.empty()) {
            cout << "second " << i;
            exit(0);
        }

        if (b.empty()) {
            cout << "first " << i;
            exit(0);
        }
    }

    cout << "botva";

    return 0;
}
