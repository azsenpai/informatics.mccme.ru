
from collections import deque

class Queue:
    def __init__(self):
        self.__q = deque()

    def push(self, n):
        self.__q.append(n)

    def pop(self):
        return self.__q.popleft()

    def front(self):
        return self.__q[0]

    def size(self):
        return len(self.__q)

    def clear(self):
        self.__q.clear()

q = Queue()

while True:
    cmd = input().split()

    if cmd[0] == 'push':
        q.push(int(cmd[1]))
        print('ok')
    elif cmd[0] == 'pop':
        if q.size() > 0:
            print(q.pop())
        else:
            print('error')
    elif cmd[0] == 'front':
        if q.size() > 0:
            print(q.front())
        else:
            print('error')
    elif cmd[0] == 'size':
        print(q.size())
    elif cmd[0] == 'clear':
        q.clear()
        print('ok')
    elif cmd[0] == 'exit':
        print('bye')
        break
