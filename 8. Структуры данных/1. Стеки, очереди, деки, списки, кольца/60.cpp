#include <iostream>
#include <deque>

using namespace std;

deque<int> dq;

int main()
{
    string cmd;
    int n;

    while (true) {
        cin >> cmd;

        if (cmd == "push_front") {
            cin >> n;
            dq.push_front(n);
            cout << "ok" << endl;
        }
        else if (cmd == "push_back") {
            cin >> n;
            dq.push_back(n);
            cout << "ok" << endl;
        }
        else if (cmd == "pop_front") {
            cout << dq.front() << endl;
            dq.pop_front();
        }
        else if (cmd == "pop_back") {
            cout << dq.back() << endl;
            dq.pop_back();
        }
        else if (cmd == "front") {
            cout << dq.front() << endl;
        }
        else if (cmd == "back") {
            cout << dq.back() << endl;
        }
        else if (cmd == "size") {
            cout << dq.size() << endl;
        }
        else if (cmd == "clear") {
            dq.clear();
            cout << "ok" << endl;
        }
        else if (cmd == "exit") {
            cout << "bye" << endl;
            break;
        }
    }

    return 0;
}
