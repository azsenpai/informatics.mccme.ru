
n = int(input())

st = [[] for i in range(n)]
path = []

flag = True

for i in range(n):
    st[i] = list(map(lambda x: int(x)-1, input().split()))[1:]
    for x in st[i]:
        if x != i: flag = False

if flag: exit()

a = 0
b = n-2
c = n-1

for i in range(1, n):
    while len(st[i]) > 0:
        st[a].append(st[i].pop())
        path.append([i, a])

while len(st[a]) > 0:
    x = st[a].pop()
    if x != a:
        st[x].append(x)
        path.append([a, x])
    else:
        st[c].append(x)
        path.append([a, c])

if c > 0:
    while len(st[c]) > 0:
        x = st[c].pop()
        if x == a:
            st[a].append(x)
            path.append([c, a])
        else:
            st[b].append(x)
            path.append([c, b])

if b > 0:
    while len(st[b]) > 0:
        x = st[b][-1]
        if x == c:
            st[c].append(x)
            path.append([b, c])
            st[b].pop()
        else:
            break

for i in range(n):
    while len(st[i]) > 0:
        if st[i].pop() != i:
            print(0)
            exit()

for i in range(len(path)):
    print(path[i][0]+1, path[i][1]+1)
