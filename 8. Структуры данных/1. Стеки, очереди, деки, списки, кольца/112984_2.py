
n = int(input())

next = [None]*(n + 1)
prev = [None]*(n + 1)

val = [None]*(n + 1)

f = None # pointer to first
l = None # pointer to last

i = 0 # list size
j = 0

for k in range(n):
    s = input().split()

    if s[0] == '+':
        next[j] = j
        prev[j] = j

        val[j] = int(s[1])

        i += 1

        if i == 1:
            f = l = j
        else:
            prev[j] = l
            prev[f] = j

            next[l] = j
            next[j] = f

            l = j
            
        j += 1
    elif s[0] == '*':
        m = f
        for g in range((i + 1) // 2):
            m = next[m]

        if m != None:
            next[j] = m
            prev[j] = prev[m]
            next[prev[j]] = j
            prev[m] = j
        else:
            next[j] = j
            prev[j] = j

        val[j] = int(s[1])

        i += 1

        if i == 1: f = l = j
            
        j += 1

    elif s[0] == '-':
        print(val[f])

        prev[next[f]] = prev[f];
        f = next[f]

        i -= 1

        if i == 0: f = l = None
