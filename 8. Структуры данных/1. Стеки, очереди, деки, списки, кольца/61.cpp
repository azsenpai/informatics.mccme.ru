#include <iostream>
#include <deque>

using namespace std;

deque<int> dq;

int main()
{
    string cmd;
    int n;

    while (true) {
        cin >> cmd;

        if (cmd == "push_front") {
            cin >> n;
            dq.push_front(n);
            cout << "ok" << endl;
        }
        else if (cmd == "push_back") {
            cin >> n;
            dq.push_back(n);
            cout << "ok" << endl;
        }
        else if (cmd == "pop_front") {
            if (!dq.empty()) {
                cout << dq.front() << endl;
                dq.pop_front();
            } else {
                cout << "error" << endl;
            }
        }
        else if (cmd == "pop_back") {
            if (!dq.empty()) {
                cout << dq.back() << endl;
                dq.pop_back();
            } else {
                cout << "error" << endl;
            }
        }
        else if (cmd == "front") {
            if (!dq.empty()) {
                cout << dq.front() << endl;
            } else {
                cout << "error" << endl;
            }
        }
        else if (cmd == "back") {
            if (!dq.empty()) {
                cout << dq.back() << endl;
            } else {
                cout << "error" << endl;
            }
        }
        else if (cmd == "size") {
            cout << dq.size() << endl;
        }
        else if (cmd == "clear") {
            dq.clear();
            cout << "ok" << endl;
        }
        else if (cmd == "exit") {
            cout << "bye" << endl;
            break;
        }
    }

    return 0;
}
