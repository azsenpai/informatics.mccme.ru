
st = []

while True:
    cmd = input().split()

    if cmd[0] == 'push':
        st.append(int(cmd[1]))
        print('ok')
    elif cmd[0] == 'pop':
        if len(st) > 0:
            print(st.pop())
        else:
            print('error')
    elif cmd[0] == 'back':
        if len(st):
            print(st[len(st) - 1])
        else:
            print('error')
    elif cmd[0] == 'size':
        print(len(st))
    elif cmd[0] == 'clear':
        del st[:]
        print('ok')
    elif cmd[0] == 'exit':
        print('bye')
        break
