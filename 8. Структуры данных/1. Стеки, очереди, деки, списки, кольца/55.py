
class Stack:
    def __init__(self):
        self.st = []

    def push(self, n):
        self.st.append(n)

    def pop(self):
        if self.size == 0: raise
        return self.st.pop()

    def back(self):
        if self.size == 0: raise
        return self.st[len(self.st) - 1];

    def size(self):
        return len(self.st)

    def clear(self):
        del self.st[:]


st = Stack()

while True:
    cmd = input().split()

    if cmd[0] == 'push':
        st.push(int(cmd[1]))
        print('ok')
    elif cmd[0] == 'pop':
        try:
            n = st.pop()
            print(n)
        except:
            print('error')
    elif cmd[0] == 'back':
        try:
            n = st.back()
            print(n)
        except:
            print('error')
    elif cmd[0] == 'size':
        print(st.size())
    elif cmd[0] == 'clear':
        st.clear()
        print('ok')
    elif cmd[0] == 'exit':
        print('bye')
        break
