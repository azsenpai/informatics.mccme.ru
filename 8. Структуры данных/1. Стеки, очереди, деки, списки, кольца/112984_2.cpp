#include <iostream>

using namespace std;

const int MAX_N = 100000;

int n;

int val_prev[MAX_N + 1];
int val_next[MAX_N + 1];

int val[MAX_N + 1];

int main()
{
    cin >> n;

    char ch;
    int x;

    int f; // pointer to first
    int c;
    int l; // pointer to last

    int j = 0;

    int m = 0; // list length

    for (int i = 0; i < n; i ++) {
        cin >> ch;

        if (ch == '+') {
            cin >> x;

            val[j] = x;
            m ++;

            if (m == 1) {
                val_prev[j] = j;
                val_next[j] = j;
                f = j;
                c = j;
                l = j;
            } else {
                val_next[l] = j;
                val_prev[f] = j;
                val_prev[j] = l;
                val_next[j] = f;
                l = j;

                if (m == 2 || (m&1)) c = val_next[c];
            }

            j ++;
        }
        else if (ch == '*') {
            cin >> x;

            val[j] = x;
            m ++;

            if (m == 1) {
                val_prev[j] = j;
                val_next[j] = j;
                f = j;
                c = j;
                l = j;
            } else {
                val_prev[j] = val_prev[c];
                val_next[j] = c;
                val_next[val_prev[j]] = j;
                val_prev[c] = j;

                if (m == 2) l = j;

                if (!(m&1)) c = j;
            }

            j ++;
        }
        else if (ch == '-') {
            cout << val[f] << endl;

            f = val_next[f];

            val_prev[f] = l;
            val_next[l] = f;

            m --;

            if (m&1) c = val_next[c];
        }
    }

    return 0;
}
