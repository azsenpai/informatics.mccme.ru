#include <iostream>
#include <vector>

using namespace std;

vector<int> st;

int main()
{
    string cmd;
    int n;

    while (true) {
        cin >> cmd;
        if (cmd == "push") {
            cin >> n;
            st.push_back(n);
            cout << "ok" << endl;
        } else if (cmd == "pop") {
            if (st.size() > 0) {
                cout << st.back() << endl;
                st.pop_back();
            } else {
                cout << "error" << endl;
            }
        } else if (cmd == "back") {
            if (st.size() > 0) {
                cout << st.back() << endl;
            } else {
                cout << "error" << endl;
            }
        } else if (cmd == "size") {
            cout << st.size() << endl;
        } else if (cmd == "clear") {
            st.clear();
            cout << "ok" << endl;
        } else if (cmd == "exit") {
            cout << "bye" << endl;
            break;
        }
    }

    return 0;
}
