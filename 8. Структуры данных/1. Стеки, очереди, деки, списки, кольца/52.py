
a = input().split()

st = []

for b in a:
    if b == '*':
        st.append(st.pop() * st.pop())
    elif b == '+':
        st.append(st.pop() + st.pop())
    elif b == '-':
        l = st.pop()
        r = st.pop()
        st.append(r - l)
    else:
        st.append(int(b))
    
print(st[0])
