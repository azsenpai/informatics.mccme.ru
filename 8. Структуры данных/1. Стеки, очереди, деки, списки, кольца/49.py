
import sys

a = [[] for i in range(3)]

for line in sys.stdin:
    s = line.split()
    a[int(s[0]) - 9].append(s[1])

for i in range(3):
    for s in a[i]: print(i+9, s)
