
s = input()
st = []

for c in s:
    if c == '(' or c == '{' or c == '[':
        st.append(c)
    elif c == ')':
        if len(st) == 0 or st[-1] != '(':
            st.append(c)
            break
        st.pop()
    elif c == '}':
        if len(st) == 0 or st[-1] != '{':
            st.append(c)
            break
        st.pop()
    elif c == ']':
        if len(st) == 0 or st[-1] != '[':
            st.append(c)
            break
        st.pop()

print('yes' if len(st) == 0 else 'no')
