
from collections import deque

a = deque(map(int, input().split()))
b = deque(map(int, input().split()))

w = None
c = 0

for i in range(1, 1000000 + 1):
    a0 = a.popleft()
    b0 = b.popleft()

    if a0 > b0 or (a0 == 0 and b0 == 9):
        a.append(a0)
        a.append(b0)
    else:
        b.append(a0)
        b.append(b0)

    if len(a) == 0:
        win = 'second'
        c = i
        break

    if len(b) == 0:
        win = 'first'
        c = i
        break
 
if win == None:
    print('botva')
else:
    print(win, c)
