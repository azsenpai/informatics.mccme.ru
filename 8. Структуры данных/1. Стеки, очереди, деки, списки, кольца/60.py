
from collections import deque

class Deque:
    def __init__(self):
        self.__dq = deque()

    def push_front(self, n):
        self.__dq.appendleft(n)

    def push_back(self, n):
        self.__dq.append(n)

    def pop_front(self):
        return self.__dq.popleft()

    def pop_back(self):
        return self.__dq.pop()

    def front(self):
        return self.__dq[0]

    def back(self):
        return self.__dq[len(self.__dq) - 1]

    def size(self):
        return len(self.__dq)

    def clear(self):
        self.__dq.clear()


dq = Deque()

while True:
    cmd = input().split()

    if cmd[0] == 'push_front':
        dq.push_front(int(cmd[1]))
        print('ok')
    elif cmd[0] == 'push_back':
        dq.push_back(int(cmd[1]))
        print('ok')
    elif cmd[0] == 'pop_front':
        print(dq.pop_front())
    elif cmd[0] == 'pop_back':
        print(dq.pop_back())
    elif cmd[0] == 'front':
        print(dq.front())
    elif cmd[0] == 'back':
        print(dq.back())
    elif cmd[0] == 'size':
        print(dq.size())
    elif cmd[0] == 'clear':
        dq.clear()
        print('ok')
    elif cmd[0] == 'exit':
        print('bye')
        break
