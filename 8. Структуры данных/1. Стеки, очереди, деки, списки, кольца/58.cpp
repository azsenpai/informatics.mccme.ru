#include <iostream>
#include <queue>

using namespace std;

queue<int> q;

int main()
{
    string cmd;
    int n;

    while (true) {
        cin >> cmd;
        if (cmd == "push") {
            cin >> n;
            q.push(n);
            cout << "ok" << endl;
        } else if (cmd == "pop") {
            if (!q.empty()) {
                cout << q.front() << endl;
                q.pop();
            } else {
                cout << "error" << endl;
            }
        } else if (cmd == "front") {
            if (!q.empty()) {
                cout << q.front() << endl;
            } else {
                cout << "error" << endl;
            }
        } else if (cmd == "size") {
            cout << q.size() << endl;
        } else if (cmd == "clear") {
            while (!q.empty()) {
                q.pop();
            }
            cout << "ok" << endl;
        } else if (cmd == "exit") {
            cout << "bye" << endl;
            break;
        }
    }

    return 0;
}
