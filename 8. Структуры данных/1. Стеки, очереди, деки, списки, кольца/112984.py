
from collections import deque

n = int(input())

next = [0]*(n+1)

for i in range(n):
    s = input().split()

    if s[0] == '+':
        dq.append(int(s[1]))
    elif s[0] == '*':
        dq.insert(len(dq)//2, int(s[1]))
    elif s[0] == '-':
        print(dq.popleft())
