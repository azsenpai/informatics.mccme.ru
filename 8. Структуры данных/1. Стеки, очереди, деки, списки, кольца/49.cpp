#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

vector<string> a[3];

int main()
{
    int i;
    string s;

    while (cin >> i >> s) {
        a[i-9].push_back(s);
    }

    for (int i = 0; i < 3; i ++) {
        for (int j = 0; j < a[i].size(); j ++) {
            cout << i+9 << " " << a[i][j] << endl;
        }
    }

    return 0;
}
