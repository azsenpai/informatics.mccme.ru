#include <iostream>
#include <deque>

using namespace std;

int n;
deque<int> dq;

int main()
{
    cin >> n;

    char c;
    int x;

    for (int i = 0; i < n; i ++) {
        cin >> c;

        if (c == '+') {
            cin >> x;
            dq.push_back(x);
        }
        else if (c == '*') {
            cin >> x;
            if (dq.empty()) {
                dq.push_back(x);
            } else {
                dq.insert(dq.begin() + (dq.size() + 1)/2, x);
            }
        }
        else if (c == '-') {
            cout << dq.front() << endl;
            dq.pop_front();
        }
    }

    return 0;
}
