
class BST:
    def __init__(self, v, h = 1):
        self.v = v
        self.h = h
        self.l = None
        self.r = None

    def __str__(self):
        return '%d %d' % (self.v, self.h)

    @staticmethod
    def add(root, v, h = 1):
        if root == None:
            return BST(v, h)
        elif v < root.v:
            if root.l == None:
                root.l = BST(v, h+1)
                return root.l
            else:
                return BST.add(root.l, v, h+1)
        elif v > root.v:
            if root.r == None:
                root.r = BST(v, h+1)
                return root.r
            else:
                return BST.add(root.r, v, h+1)

root = None
node = None

a = list(map(int, input().split()))
h = 0

for x in a:
    if x == 0: break

    node = BST.add(root, x)
    if root == None: root = node

    if node != None: h = max(h, node.h)

print(h)
