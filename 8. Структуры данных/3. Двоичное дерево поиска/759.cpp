#include <iostream>

using namespace std;

class BST;
typedef BST* pBST;

class BST
{
    public:
        int v;
        int h;

        pBST p;
        pBST l;
        pBST r;

        BST(int _v, int _h): v(_v), h(_h)
        {
            p = NULL;
            l = NULL;
            r = NULL;
        }
};

void bst_add(pBST &n, int v, int h = 1)
{
    if (n == NULL) {
        n = new BST(v, h);
    }
    else if (v < n->v) {
        if (n->l == NULL) {
            n->l = new BST(v, h+1);
            n->l->p = n;
        } else {
            bst_add(n->l, v, h+1);
        }
    }
    else if (v > n->v) {
        if (n->r == NULL) {
            n->r = new BST(v, h+1);
            n->r->p = n;
        } else {
            bst_add(n->r, v, h+1);
        }
    }
}

pBST bst_rightmost(pBST n)
{
    while (n->r != NULL) {
        n = n->r;
    }

    return n;
}

pBST root = NULL;

int main()
{
    int x;

    while (cin >> x) {
        if (x == 0) break;

        bst_add(root, x);
    }

    pBST node = bst_rightmost(root);

    if (node->l != NULL) {
        node = bst_rightmost(node->l);
    } else {
        node = node->p;
    }

    cout << node->v;

    return 0;
}
