#include <iostream>

using namespace std;

class BST;
typedef BST* pBST;

class BST
{
    public:
        int v;
        int h;
        int c;

        pBST p;
        pBST l;
        pBST r;

        BST(int _v, int _c = 1): v(_v), c(_c)
        {
            p = NULL;
            l = NULL;
            r = NULL;
        }

        bool is_leaf()
        {
            return l == NULL && r == NULL;
        }

        bool is_fork()
        {
            return l != NULL && r != NULL;
        }

        bool is_branch()
        {
            return (l != NULL) ^ (r != NULL);
        }
};

void bst_add(pBST &n, int v)
{
    if (n == NULL) {
        n = new BST(v);
    }
    else if (v < n->v) {
        if (n->l == NULL) {
            n->l = new BST(v);
            n->l->p = n;
        } else {
            bst_add(n->l, v);
        }
    }
    else if (v > n->v) {
        if (n->r == NULL) {
            n->r = new BST(v);
            n->r->p = n;
        } else {
            bst_add(n->r, v);
        }
    } else {
        n->c ++;
    }
}

pBST bst_rightmost(pBST n)
{
    while (n->r != NULL) {
        n = n->r;
    }

    return n;
}

int max_diff = 0;

void bst_inorder_walk(pBST n)
{
    if (n != NULL) {
        bst_inorder_walk(n->l);
        cout << n->v << ' ' << n->c << endl;
        bst_inorder_walk(n->r);
    }
}

void calc_height(pBST n)
{
    if (n->is_leaf()) {
        n->h = 1;
    } else {
        int hl = 1;
        int hr = 1;

        if (n->l != NULL) {
            calc_height(n->l);
            hl = n->l->h + 1;
        }

        if (n->r != NULL) {
            calc_height(n->r);
            hr = n->r->h + 1;
        }

        n->h = max(hl, hr);

        max_diff = max(max_diff, abs(hl - hr));
    }
}

pBST root = NULL;

int main()
{
    int x;

    while (cin >> x) {
        if (x == 0) break;
        bst_add(root, x);
    }

    bst_inorder_walk(root);

    return 0;
}
