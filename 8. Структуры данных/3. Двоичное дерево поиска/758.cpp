#include <iostream>

using namespace std;

class BST;
typedef BST* pBST;

class BST
{
    public:
        int v;
        int h;

        pBST l;
        pBST r;

        BST(int _v, int _h = 1): v(_v), h(_h)
        {
            l = NULL;
            r = NULL;
        }
};

pBST bst_add(pBST n, int v, int h = 1)
{
    if (n == NULL) {
        return new BST(v, h);
    }
    else if (v < n->v) {
        if (n->l == NULL) {
            n->l = new BST(v, h+1);
            return n->l;
        }
        return bst_add(n->l, v, h+1);
    }
    else if (v > n->v) {
        if (n->r == NULL) {
            n->r = new BST(v, h+1);
            return n->r;
        }
        return bst_add(n->r, v, h+1);
    }

    return NULL;
}

pBST root = NULL;
pBST node = NULL;

int main()
{
    int c = 0;
    int x;

    while (cin >> x) {
        if (x == 0) break;

        node = bst_add(root, x);

        if (node != NULL) {
            if (root == NULL) root = node;
            c ++;
        }
    }

    cout << c;

    return 0;
}
