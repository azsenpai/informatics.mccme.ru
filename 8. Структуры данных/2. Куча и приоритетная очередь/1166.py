
from os import linesep

def siftup(heap, i):
    p = (i - 1) >> 1

    while i > 0 and heap[p] < heap[i]:
        heap[p], heap[i] = heap[i], heap[p]
        i = p
        p = (i - 1) >> 1

    return i

def siftdown(heap, i):
    n = len(heap)

    while True:
        p = i
        l = (i << 1) + 1
        r = (i << 1) + 2

        if l < n and heap[p] < heap[l]: p = l
        if r < n and heap[p] < heap[r]: p = r

        if p == i: break

        heap[i], heap[p] = heap[p], heap[i]
        i = p

    return i

def pop(heap):
    m = heap[0]
    heap[0] = heap.pop()

    i = siftdown(heap, 0)

    return (i+1, m)

n = int(input())
#a = list(map(int, input().split()))

a = input().split()
for i in range(n): a[i] = int(a[i])

out = ''

for k in range(1, n):
    m = pop(a)

    out += '%d %d' % m
    out += linesep

print(out)
