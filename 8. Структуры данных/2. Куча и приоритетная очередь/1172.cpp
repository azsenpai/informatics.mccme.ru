#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;

int n;
int m;

vi a;
vi b;

int sift_up(vi &heap, int i)
{
    int p = (i-1) >> 1;

    while (i > 0 && heap[p] < heap[i]) {
        swap(heap[p], heap[i]);
        i = p;
        p = (i-1) >> 1;
    }

    return i;
}

int sift_down(vi &heap, int i)
{
    int s = heap.size();
    int p, l, r;

    while (true) {
        p = i;
        l = (p << 1) + 1;
        r = (p << 1) + 2;

        if (l < s && heap[p] < heap[l]) p = l;
        if (r < s && heap[p] < heap[r]) p = r;

        if (p == i) break;

        swap(heap[p], heap[i]);

        i = p;
    }

    return i;
}

pii pop(vi &heap)
{
    int x = heap[0];

    heap[0] = heap.back();
    heap.pop_back();

    int i = heap.empty() ? -1 : sift_down(heap, 0);

    return make_pair(i, x);
}

int pop(vi &heap, int i)
{
    if (i >= heap.size() || i < 0) {
        return -1;
    }

    int x = heap[i];

    heap[i] = heap.back();
    heap.pop_back();

    sift_up(heap, i);
    sift_down(heap, i);

    return x;
}

int insert(vi &heap, int x)
{
    heap.push_back(x);
    return sift_up(heap, heap.size() - 1);
}

void build_heap2(vi &heap)
{
    for (int i = (heap.size() - 1)/2; i >= 0; i --) {
        sift_down(heap, i);
    }
}

int main()
{
    cin >> n;
    a.resize(n);

    for (int i = 0; i < n; i ++) {
        cin >> a[i];
    }

    build_heap2(a);
    pii m;

    for (int i = 0; i < n; i ++) {
        m = pop(a);
        b.push_back(m.second);
    }

    for (int i = n-1; i >= 0; i --) {
        cout << b[i] << ' ';
    }

    return 0;
}
