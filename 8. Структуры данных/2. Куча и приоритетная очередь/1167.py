
from collections import deque
from os import linesep


buffer = deque()

def nextint():
    global buffer

    while len(buffer) == 0:
        buffer.extend(input().split())

    return int(buffer.popleft())


def siftup(heap, i):
    p = (i - 1) >> 1

    while i > 0 and heap[p] < heap[i]:
        heap[p], heap[i] = heap[i], heap[p]
        i = p
        p = (i - 1) >> 1

    return i

def siftdown(heap, i):
    s = len(heap)

    while True:
        p = i
        l = (i << 1) + 1
        r = (i << 1) + 2

        if l < s and heap[p] < heap[l]: p = l
        if r < s and heap[p] < heap[r]: p = r

        if p == i: break

        heap[i], heap[p] = heap[p], heap[i]
        i = p

    return i

def pop(heap):
    x = heap[0]
    heap[0] = heap.pop()

    i = -1 if len(heap) == 0 else siftdown(heap, 0)

    return (i+1, x)

def insert(heap, x):
    heap.append(x)
    return siftup(heap, len(heap)-1)


#n, m = map(int, input().split())

n = nextint()
m = nextint()

a = []

out = ''

for k in range(m):
    #s = list(map(int, input().split()))
    s = [nextint(), 0]

    if s[0] == 1:
        if len(a) == 0:
            #out += '-1'
            print(-1)
        else:
            m = pop(a)
            #out += '%d %d' % m
            print('%d %d' % m)
    elif s[0] == 2:
        s[1] = nextint()

        if len(a) == n:
            #out += '-1'
            print(-1)
        else:
            #out += str(insert(a, s[1]) + 1)
            print(insert(a, s[1]) + 1)

    #out += linesep

#out += ' '.join(map(str, a))

#print(out)
