
from collections import deque
from os import linesep


buffer = deque()

def nextint():
    global buffer

    while len(buffer) == 0:
        buffer.extend(input().split())

    return int(buffer.popleft())


def siftup(heap, i):
    p = (i - 1) >> 1

    while i > 0 and heap[p] < heap[i]:
        heap[p], heap[i] = heap[i], heap[p]
        i = p
        p = (i - 1) >> 1

    return i

def siftdown(heap, i):
    s = len(heap)

    while True:
        p = i
        l = (i << 1) + 1
        r = (i << 1) + 2

        if l < s and heap[p] < heap[l]: p = l
        if r < s and heap[p] < heap[r]: p = r

        if p == i: break

        heap[i], heap[p] = heap[p], heap[i]
        i = p

    return i

def pop(heap):
    x = heap[0]

    if len(heap) > 1: heap[0] = heap.pop()

    i = -1 if len(heap) == 0 else siftdown(heap, 0)

    return (i+1, x)

def insert(heap, x):
    heap.append(x)
    return siftup(heap, len(heap)-1)

def buildheap1(heap):
    for i in range(1, len(heap)):
        siftup(heap, i)

def buildheap2(heap):
    for i in range((len(heap) - 1) // 2, -1, -1):
        siftdown(heap, i)


n = int(input())
a = list(map(int, input().split()))

buildheap2(a)

b = []

for i in range(n):
    b.append(pop(a)[1])

print(' '.join(map(str, b[::-1])))
