
from os import linesep

def siftup(heap, i):
    p = (i - 1) >> 1

    while i > 0 and heap[p] < heap[i]:
        heap[p], heap[i] = heap[i], heap[p]
        i = p
        p = (i - 1) >> 1

    return i

def siftdown(heap, i):
    n = len(heap)

    while True:
        p = i
        l = (i << 1) + 1
        r = (i << 1) + 2

        if l < n and heap[p] < heap[l]: p = l
        if r < n and heap[p] < heap[r]: p = r

        if p == i: break

        heap[i], heap[p] = heap[p], heap[i]
        i = p

    return i


n = int(input())
a = list(map(int, input().split()))

m = int(input())

out = ''

for k in range(m):
    i, x = map(int, input().split())

    i -= 1
    a[i] -= x

    out += str(siftdown(a, i) + 1)
    out += linesep

out += ' '.join(map(str, a))

print(out)
