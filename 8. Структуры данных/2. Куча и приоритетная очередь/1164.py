
from os import linesep

def siftup(heap, i):
    p = (i - 1) >> 1

    while i > 0 and heap[p] < heap[i]:
        heap[p], heap[i] = heap[i], heap[p]
        i = p
        p = (i - 1) >> 1

    return i

n = int(input())
a = list(map(int, input().split()))

m = int(input())

out = ''

for k in range(m):
    i, x = map(int, input().split())

    i -= 1
    a[i] += x

    out += str(siftup(a, i) + 1)
    out += linesep

out += ' '.join(map(str, a))

print(out)
