#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;

int n;
vi a;

int sift_down(vi &heap, int i)
{
    int s = heap.size();
    int p, l, r;

    while (true) {
        p = i;
        l = (p << 1) + 1;
        r = (p << 1) + 2;

        if (l < s && heap[p] < heap[l]) p = l;
        if (r < s && heap[p] < heap[r]) p = r;

        if (p == i) break;

        swap(heap[i], heap[p]);

        i = p;
    }

    return i;
}

pii pop(vi &heap)
{
    int m = heap[0];

    heap[0] = heap.back();
    heap.pop_back();

    int i = sift_down(heap, 0);

    return make_pair(i, m);
}

int main()
{
    cin >> n;
    a.resize(n);

    for (int i = 0; i < n; i ++) {
        cin >> a[i];
    }

    pii m;

    for (int i = 1; i < n; i ++) {
        m = pop(a);
        cout << m.first+1 << ' ' << m.second << endl;
    }

    return 0;
}
