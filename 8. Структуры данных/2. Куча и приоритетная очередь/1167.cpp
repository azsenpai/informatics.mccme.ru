#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;

int n;
int m;

vi a;

int sift_up(vi &heap, int i)
{
    int p = (i-1) >> 1;

    while (i > 0 && heap[p] < heap[i]) {
        swap(heap[p], heap[i]);
        i = p;
        p = (i-1) >> 1;
    }

    return i;
}

int sift_down(vi &heap, int i)
{
    int s = heap.size();
    int p, l, r;

    while (true) {
        p = i;
        l = (p << 1) + 1;
        r = (p << 1) + 2;

        if (l < s && heap[p] < heap[l]) p = l;
        if (r < s && heap[p] < heap[r]) p = r;

        if (p == i) break;

        swap(heap[p], heap[i]);

        i = p;
    }

    return i;
}

pii pop(vi &heap)
{
    int x = heap[0];

    heap[0] = heap.back();
    heap.pop_back();

    int i = heap.empty() ? -1 : sift_down(heap, 0);

    return make_pair(i, x);
}

int insert(vi &heap, int x)
{
    heap.push_back(x);
    return sift_up(heap, heap.size() - 1);
}

int main()
{
    cin >> n >> m;

    pii top;

    for (int i = 0, k, x; i < m; i ++) {
        cin >> k;
        if (k == 1) {
            if (a.empty()) {
                cout << -1;
            } else {
                top = pop(a);
                cout << top.first+1 << ' ' << top.second;
            }
        }
        else if (k == 2) {
            cin >> x;
            if (a.size() == n) {
                cout << -1;
            } else {
                cout << insert(a, x) + 1;
            }
        }
        cout << endl;
    }

    for (int i = 0; i < a.size(); i ++) {
        cout << a[i] << ' ';
    }

    return 0;
}
