
days = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

def isleap(y):
    return (y%4 == 0 and y%100 != 0) or (y%400 == 0)

def getd(m, y):
    if m == 2:
        return days[m] + isleap(y)
    return days[m]

d, m, y = map(int, input().split())

d += 2
c = getd(m, y)

if c < d:
    d -= c
    m += 1
    if m > 12:
        m = 1
        y += 1

print(d, m, y)
