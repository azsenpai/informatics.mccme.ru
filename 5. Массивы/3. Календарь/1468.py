
days = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

def isleap(y):
    return (y%4 == 0 and y%100 != 0) or (y%400 == 0)

def getd(m, y):
    if m == 2:
        return days[m] + isleap(y)
    return days[m]

s = input()

d = int(s[:2])
m = int(s[2:4])
y = int(s[4:])

n = d - getd(m, y)

m1 = 1
y1 = 1

while (m1 <= m and y1 <= y) or (m1 > m and y1 < y):
    n += getd(m1, y1)
    m1 += 1
    if m1 > 12:
        m1 = 1
        y1 += 1

print(n)
