
n = int(input())
d = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

s = 0
i = 0

while s < n:
    i += 1
    s += d[i]

print(d[i] - s + n, i)
