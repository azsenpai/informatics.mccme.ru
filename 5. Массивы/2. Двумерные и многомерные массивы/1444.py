
n, m = map(int, input().split())
w = int(input())

a = [[0]*m for i in range(n)]

for k in range(w):
    i, j = map(int, input().split())
    a[i-1][j-1] = -1

dx = (-1, -1, -1,  0, 0,  1, 1, 1)
dy = (-1,  0,  1, -1, 1, -1, 0, 1)

for i in range(n):
    for j in range(m):
        if a[i][j] == -1: continue
        for k in range(8):
            if 0 <= i+dx[k] < n and 0 <= j+dy[k] < m:
                a[i][j] += a[i+dx[k]][j+dy[k]] == -1

for i in range(n):
    for j in range(m):
        print('*' if a[i][j] == -1 else a[i][j], end = ' ')
    print()
