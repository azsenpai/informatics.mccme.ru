
n, m = map(int, input().split())
a = [[0]*m for i in range(n)]

for i in range(n): a[i][0] = 1
for j in range(m): a[0][j] = 1

for i in range(1, n):
    for j in range(1, m): a[i][j] = a[i-1][j] + a[i][j-1]

for i in range(n):
    print(' '.join(map(str, a[i])))
