
n = int(input())

N = 2*n + 1
a = [[0] * N for i in range(N)]

i = 0
j = N - 1

k = N*N - 1

dx = 1
dy = 0

while k >= 0:
    a[i][j] = k

    if not (0 <= i+dx < N and 0 <= j+dy < N and a[i+dx][j+dy] == 0):
        if dx == 1:
            dx = 0
            dy = -1
        elif dy == -1:
            dx = -1
            dy = 0
        elif dx == -1:
            dx = 0
            dy = 1
        else:
            dx = 1
            dy = 0

    k -= 1
    i += dx
    j += dy
        

for i in range(N):
    for j in range(N):
        print('%3d' % a[i][j], end = '')
    print()
