
n, m = map(int, input().split())

a = [0] * n
m = [0] * n
s = [0] * n

for i in range(n):
    a[i] = list(map(int, input().split()))
    m[i] = max(a[i])
    s[i] = sum(a[i])

maxv = m[0]
maxs = s[0]
maxi = 0

for i in range(1, n):
    if maxv < m[i] or (maxv == m[i] and maxs < s[i]):
        maxv = m[i]
        maxs = s[i]
        maxi = i

print(maxi)
