
n, m = map(int, input().split())

a = [0] * n

for i in range(n):
    a[i] = input().split()

print(m, n)

for j in range(m):
    for i in range(n-1, -1, -1):
        print(a[i][j], end = ' ')
    print()
