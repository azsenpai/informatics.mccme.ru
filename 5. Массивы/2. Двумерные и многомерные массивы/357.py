
n, m = map(int, input().split())

if n == 0: exit()

a = [0] * n
v = [0] * n

for i in range(n):
    a[i] = list(map(int, input().split()))
    v[i] = max(a[i])

maxv = v[0]
maxi = 0
maxj = 0

for j in range(0, m):
    if maxv == a[0][j]:
        maxj = j
        break

for i in range(1, n):
    if maxv < v[i]:
        maxv = v[i]
        maxi = i
        for j in range(m):
            if maxv == a[i][j]:
                maxj = j
                break

print(maxv)
print(maxi, maxj)
