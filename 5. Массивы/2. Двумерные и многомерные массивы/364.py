
n, m = map(int, input().split())

a = [[0]*m for i in range(n)]
k = 1

for s in range(1, n*m):
    for i in range(n):
        for j in range(m):
            if i + j == s:
                a[i][j] = k
                k += 1

for i in range(n):
    for j in range(m):
        print('%3d' % a[i][j], end = '')
    print()
