
n = int(input())
a = [0] * n

for i in range(n): a[i] = input().split()

yes = True

for i in range(n):
    for j in range(n):
        if a[i][j] != a[j][i]:
            yes = False
            break
    if not yes: break

print('yes' if yes else 'no')
