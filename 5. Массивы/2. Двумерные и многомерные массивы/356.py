
n, m = map(int, input().split())

a = [0] * n
s = [0] * n

for i in range(n):
    a[i] = list(map(int, input().split()))
    s[i] = sum(a[i])

maxs = s[0]
maxi = 0

for i in range(1, n):
    if maxs < s[i]:
        maxs = s[i]
        maxi = i

print(maxs)
print(maxi)
