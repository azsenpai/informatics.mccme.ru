
n = int(input())
a = [[0] * n for i in range(n)]

dx = (0, 1,  0, -1)
dy = (1, 0, -1,  0)

i = 0
j = 0
k = 0

def f(i, j, k):
    return not (0 <= i+dx[k] < n and 0 <= j+dy[k] < n) \
        or (a[i+dx[k]][j+dy[k]] != 0) \
        or (0 <= i+2*dx[k] < n and 0 <= j+2*dy[k] < n and a[i+2*dx[k]][j+2*dy[k]] != 0)

while True:
    a[i][j] = 1

    if f(i, j, k):
        k = (k + 1) % 4
        if f(i, j, k): break

    i += dx[k]
    j += dy[k]

for i in range(n):
    print(''.join(map(str, a[i])))
