
n = int(input())

a = [0] * n
for i in range(n): a[i] = [0] * n

for i in range(n):
    for j in range(n):
        if j == n-i-1:
            a[i][j] = 1
        elif n-i-1 < j:
            a[i][j] = 2

for i in range(n):
    print(' '.join(map(str, a[i])))
