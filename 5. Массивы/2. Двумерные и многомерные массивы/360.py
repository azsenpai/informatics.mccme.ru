
n, m = map(int, input().split())

a = [0] * n
m = [0] * n

for i in range(n):
    a[i] = list(map(int, input().split()))
    m[i] = max(a[i])

maxv = max(m)
c = 0

for i in range(0, n):
    c += maxv == m[i]

print(c)

for i in range(0, n):
    if maxv == m[i]: print(i)
