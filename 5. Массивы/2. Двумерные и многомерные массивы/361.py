
n, m = map(int, input().split())
a = [[0]*m for i in range(n)]

i = 0
j = 0

while i < n and j < m:
    a[i][j] = i*j
    j += 1

    if j == m:
        j = 0
        i += 1

for i in range(n):
    #for j in range(m):
    #    a[i][j] = i*j
    print(' '.join(map(str, a[i])))
