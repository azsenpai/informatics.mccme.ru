
n, m = map(int, input().split())

a = [[0]*m for i in range(n)]
k = 0

for i in range(n):
    l, r, s  = (0, m, 1) if i%2 == 0 else (m-1, -1, -1)

    for j in range(l, r, s):
        a[i][j] = k
        k += 1

for i in range(n):
    for j in range(m):
        print('%3d' % a[i][j], end = '')
    print()
