
a = list(map(int, input().split()))
m = None

for i in range(len(a)):
    if a[i] % 2 == 1 and (m == None or a[i] < m): m = a[i]

print(0 if m == None else m)
