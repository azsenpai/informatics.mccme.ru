
n = int(input())
a = list(map(int, input().split()))

c = 0

for i in range(n):
    if i - 1 >= 0: c += a[i-1] < a[i]

print(c)
