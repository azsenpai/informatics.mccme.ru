
n = int(input())
a = list(map(int, input().split()))
h = int(input())

for i in range(n):
    if h > a[i]:
        print(i+1)
        break
else:
    print(n+1)
