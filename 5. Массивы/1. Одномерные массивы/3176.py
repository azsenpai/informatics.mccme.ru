
x = [0] * 8
y = [0] * 8

for i in range(8):
    x[i], y[i] = map(int, input().split())

yes = False

for i in range(8):
    for j in range(i+1, 8):
        if abs(x[i] - x[j]) == abs(y[i] - y[j]) or x[i] == x[j] or y[i] == y[j]:
            yes = True
            break
    if yes: break

print('YES' if yes else 'NO')
