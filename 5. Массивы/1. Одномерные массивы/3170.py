
a = list(map(int, input().split()))
c = 0

for i in range(len(a)):
    for j in range(i+1, len(a)):
        c += a[i] == a[j]

print(c)
