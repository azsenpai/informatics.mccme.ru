
n = int(input())
a = list(map(int, input().split()))
k = int(input())

if k >= 0:
    k %= n
    a = a[-k:] + a[:-k]
else:
    k = abs(k)
    k %= n
    a = a[k:] + a[:k]

print(' '.join(map(str, a)))
