
n, a, b, c, d = map(int, input().split())

a -= 1
b -= 1
c -= 1
d -= 1

l = []

for i in range(n): l.append(i + 1)

l[a:b+1] = l[b:a-1 if a > 0 else None:-1]
l[c:d+1] = l[d:c-1 if c > 0 else None:-1]

print(' '.join(map(str, l)))
