
a = list(map(int, input().split()))
d = dict.fromkeys(a, 0)

for i in range(len(a)):
    d[a[i]] += 1

print(max(d, key = d.get))
