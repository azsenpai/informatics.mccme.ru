
a = list(map(int, input().split()))

maxv = None
maxi = None

minv = None
mini = None

for i in range(len(a)):
    if minv == None or minv > a[i]:
        minv = a[i]
        mini = i

    if maxv == None or maxv < a[i]:
        maxv = a[i]
        maxi = i

a[mini], a[maxi] = a[maxi], a[mini]

print(' '.join(map(str, a)))
