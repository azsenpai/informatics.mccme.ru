
a = list(map(int, input().split()))

n = a[0]
a = a[1:]

while True:
    i = None
    j = None
    
    for k in range(1, len(a)):
        if a[k-1] == a[k]:
            if i == None: i = k-1
            else: j = k
        else:
            if i == None or j == None or (j-i+1 < 2):
                i = None
                j = None
            else:
                break

    if i == None or j == None: break
    a = a[:i] + a[j+1:]

print(n - len(a))
