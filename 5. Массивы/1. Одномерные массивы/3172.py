
a = list(map(int, input().split()))
d = dict.fromkeys(a, 1)
c = 0

for i in range(len(a)):
    c += d[a[i]]; d[a[i]] = 0

print(c)
