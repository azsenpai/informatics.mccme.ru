
a = list(map(int, input().split()))
c = 0

for i in range(len(a)):
    if a[i] == 0:
        c += 1
    else:
        print(a[i], end = ' ')

print('0 ' * c)
