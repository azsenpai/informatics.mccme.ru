
a = list(map(int, input().split()))
c = 1

for i in range(1, len(a)): c += a[i-1] != a[i]

print(c)
