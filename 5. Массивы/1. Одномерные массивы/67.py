
n = int(input())
a = list(map(int, input().split()))

yes = False

for i in range(1, n):
    if a[i-1] * a[i] > 0:
        yes = True
        break

print('YES' if yes else 'NO')
