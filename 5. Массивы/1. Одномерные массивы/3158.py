
a = list(map(int, input().split()))

m = a[0]
j = 0

for i in range(1, len(a)):
    if m < a[i]:
        m = a[i]
        j = i

print(m, j)
