
n = int(input())

h = (n // 3600) % 24
n %= 3600

m = n // 60
n %= 60

s = n

print('%d:%02d:%02d' % (h, m, s))
