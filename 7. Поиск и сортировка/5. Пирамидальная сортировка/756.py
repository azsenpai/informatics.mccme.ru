
def heapcmp(a, b):
    return a[0] <= b[0]

def heapinsert(heap, x, index, place):
    heap.append([x, index])
    place[index] = len(heap) - 1

    if len(heap) > 1: siftdown(heap, place[index], place)

def heappop(heap, place):
    if len(heap) > 0:
        x = heap[0][0]

        heap[0] = heap[len(heap)-1]
        place[heap[0][1]] = 0

        heap.pop()
        siftup(heap, 0, place)

        return x

def siftup(heap, i, place):
    pos = i

    pos1 = (i << 1) + 1
    pos2 = (i << 1) + 2

    if pos1 < len(heap) and not heapcmp(heap[pos], heap[pos1]): pos = pos1
    if pos2 < len(heap) and not heapcmp(heap[pos], heap[pos2]): pos = pos2

    if pos != i:
        heap[i], heap[pos] = heap[pos], heap[i]

        place[heap[pos][1]] = pos
        place[heap[i][1]] = i

        siftup(heap, pos, place)

def siftdown(heap, i, place):
    p = (i - 1) >> 1

    while i > 0 and not heapcmp(heap[p], heap[i]):
        heap[p], heap[i] = heap[i], heap[p]

        place[heap[i][1]] = i
        place[heap[p][1]] = p

        i = p
        p = (i - 1) >> 1


n, k = map(int, input().split())

a = list(map(int, input().split()))
b = []

place = [0]*n

for i in range(k): heapinsert(b, a[i], i, place)

for i in range(n-k+1):
    print(b[0][0])

    b[place[i]][0] = -(1 << 30)
    siftdown(b, place[i], place)

    heappop(b, place)

    if k+i < n: heapinsert(b, a[k+i], k+i, place)
