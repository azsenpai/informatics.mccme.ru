
def heapinsert(heap, x):
    heap.append(x)

    if len(heap) > 1:
        i = len(heap) - 1
        p = (i - 1) >> 1

        while i > 0 and heap[p] < heap[i]:
            heap[p], heap[i] = heap[i], heap[p]
            i = p
            p = (i - 1) >> 1

def heappop(heap):
    if len(heap) > 0:
        x = heap[0]
        heap[0] = heap[len(heap)-1]

        heap.pop()
        siftup(heap, 0)

        return x

def siftup(heap, i):
    pos = i

    pos1 = (i << 1) + 1
    pos2 = (i << 1) + 2

    if pos1 < len(heap) and heap[pos] < heap[pos1]: pos = pos1
    if pos2 < len(heap) and heap[pos] < heap[pos2]: pos = pos2

    if pos != i:
        heap[i], heap[pos] = heap[pos], heap[i]
        siftup(heap, pos)


n = int(input())
a = []

for i in range(n):
    cmd = input().split()

    if len(cmd) == 2:
        heapinsert(a, int(cmd[1]))
    else:
        print(heappop(a))
