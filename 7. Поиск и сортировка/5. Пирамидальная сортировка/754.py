
import heapq

n = int(input())
a = list(map(int, input().split()))

heapq.heapify(a)

for i in range(n):
    print(heapq.heappop(a), end = ' ')
