
def heappop(heap):
    if len(heap) > 0:
        x = heap[0]
        heap[0] = heap[len(heap)-1]

        heap.pop()
        siftup(heap, 0)

        return x

def siftup(heap, i):
    pos = i

    pos1 = (i << 1) + 1
    pos2 = (i << 1) + 2

    if pos1 < len(heap) and heap[pos] > heap[pos1]: pos = pos1
    if pos2 < len(heap) and heap[pos] > heap[pos2]: pos = pos2

    if pos != i:
        heap[i], heap[pos] = heap[pos], heap[i]
        siftup(heap, pos)

def heapify(heap):
    for i in range((n - 1) >> 1, -1, -1):
        siftup(heap, i)

n = int(input())
a = list(map(int, input().split()))

heapify(a)

for i in range(n):
    print(heappop(a), end = ' ')
