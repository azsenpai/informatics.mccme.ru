#include <iostream>

using namespace std;

typedef pair<int, int> pii;

const int MAX_N = 150000;
const int MAX_K = 10000 + 1;

int n;
int k;

int a[MAX_N];
int place[MAX_N];

int heap_size = 0;
pii heap[MAX_K];

bool heap_cmp(pii a, pii b)
{
    return a.first <= b.first;
}

void sift_down(int i)
{
    int p = (i-1) >> 1;

    while (i > 0 && !heap_cmp(heap[p], heap[i])) {
        swap(heap[p], heap[i]);

        place[heap[p].second] = p;
        place[heap[i].second] = i;

        i = p;
        p = (i-1) >> 1;
    }
}

void sift_up(int i)
{
    int pos = i;
    int pos1 = (i << 1) + 1;
    int pos2 = (i << 1) + 2;

    if (pos1 < heap_size && !heap_cmp(heap[pos], heap[pos1])) {
        pos = pos1;
    }
    if (pos2 < heap_size && !heap_cmp(heap[pos], heap[pos2])) {
        pos = pos2;
    }

    if (pos != i) {
        swap(heap[pos], heap[i]);

        place[heap[i].second] = i;
        place[heap[pos].second] = pos;

        sift_up(pos);
    }
}

void heap_insert(int x, int i)
{
    heap[heap_size] = make_pair(x, i);
    heap_size ++;

    place[i] = heap_size - 1;
    sift_down(place[i]);
}

int heap_pop()
{
    int x = heap[0].first;
    heap[0] = heap[heap_size - 1];

    heap_size --;

    place[heap[0].second] = 0;
    sift_up(0);

    return x;
}

int main()
{
    cin >> n >> k;

    for (int i = 0; i < n; i ++) {
        cin >> a[i];
    }

    for (int i = 0; i < k; i ++) {
        heap_insert(a[i], i);
    }

    for (int i = 0, c = n-k+1; i < c; i ++) {
        cout << heap[0].first << endl;

        heap[place[i]].first = -(1 << 30);
        sift_down(place[i]);

        heap_pop();

        if (k + i < n) {
            heap_insert(a[k+i], k+i);
        }
    }

    return 0;
}
