
n = int(input())
a = list(map(int, input().split()))
x = int(input())

print('YES' if a.count(x) > 0 else 'NO')
