
a = list(map(int, input().split()))
del a[0]

maxv = max(a)
minv = min(a)

for x in a:
    if x == maxv: print(minv, end = ' ')
    else: print(x, end = ' ')
