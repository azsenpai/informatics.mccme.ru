
n = int(input())
a = list(map(int, input().split()))

max1 = None
max2 = None

for x in a:
    if max1 == None or max1 < x:
        max1, max2 = x, max1
    elif max1 != x and (max2 == None or max2 < x):
        max2 = x

print(max2)
