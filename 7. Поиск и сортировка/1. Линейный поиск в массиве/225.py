
n = int(input())
a = list(map(int, input().split()))
x = int(input())

y = None

for t in a:
    if y == None or abs(y-x) > abs(t-x): y = t

print(y)
