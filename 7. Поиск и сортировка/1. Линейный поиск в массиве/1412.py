
x = int(input())
n = int(input())

a = [0]*n

for i in range(n):
    a[i] = list(map(int, input().split()))

for j in range(n):
    is_good = False
    for i in range(n):
        if x == a[i][j]:
            is_good = True
            break
    print('YES' if is_good else 'NO')
