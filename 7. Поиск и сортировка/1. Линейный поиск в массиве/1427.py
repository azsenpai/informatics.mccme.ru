
n, m = map(int, input().split())

a = [0]*n

for i in range(n):
    a[i] = list(map(int, input().split()))

minv = [0]*n
maxv = [0]*m

for i in range(n):
    minv[i] = None
    for j in range(m):
        if minv[i] == None or minv[i] > a[i][j]:
            minv[i] = a[i][j]

for j in range(m):
    maxv[j] = None
    for i in range(n):
        if maxv[j] == None or maxv[j] < a[i][j]:
            maxv[j] = a[i][j]

k = 0

for i in range(n):
    for j in range(m):
        k += minv[i] == a[i][j] == maxv[j]

print(k)
