
n = int(input())
a = list(map(int, input().split()))

min1 = None
min2 = None

for x in a:
    if min1 == None or min1 > x:
        min1, min2 = x, min1
    elif min2 == None or min2 > x:
        min2 = x

print(min1, min2)
