
def qsort(l, r):
    global a

    x = a[(l + r) // 2]
    i = l
    j = r

    while i < j:
        while a[i] < x: i += 1
        while a[j] > x: j -= 1

        if i <= j:
            a[i], a[j] = a[j], a[i]
            i += 1
            j -= 1

    if i < r: qsort(i, r)
    if l < j: qsort(l, j)

n = int(input())
a = list(map(int, input().split()))

qsort(0, n-1)

print(' '.join(map(str, a)))
