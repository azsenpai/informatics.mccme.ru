
n = int(input())
a = list(map(int, input().split()))

m = int(input())
b = list(map(int, input().split()))

a.sort()
b.sort()

i = 0
j = 0

while i < n and j < m:
    if a[i] != b[j]: break

    while i+1 < n and a[i] == a[i+1]: i += 1
    while j+1 < m and b[j] == b[j+1]: j += 1

    i += 1
    j += 1

print('YES' if i == n and j == m else 'NO')
