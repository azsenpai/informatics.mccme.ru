
def pow2(i):
    return i*i

def pow3(i):
    return i*i*i

x = int(input())

i = 1
j = 1

for k in range(x):
    a = pow2(i)
    b = pow3(j)

    if a < b:
        i += 1
    elif a > b:
        j += 1
    else:
        i += 1
        j += 1

print(a if a < b else b)
