
def mergelist(a, l, m, r):
    i = l
    j = m + 1

    b = []

    while i <= m and j <= r:
        if a[i] < a[j]:
            b.append(a[i])
            i += 1
        else:
            b.append(a[j])
            j += 1

    while i <= m:
        b.append(a[i])
        i += 1

    while j <= r:
        b.append(a[j])
        j += 1

    for k in range(len(b)): a[l+k] = b[k]

def mergesort(a, l, r):
    if l < r:
        m = (l + r) // 2

        mergesort(a, l, m)
        mergesort(a, m+1, r)

        mergelist(a, l, m, r)


n = int(input())
a = list(map(int, input().split()))

mergesort(a, 0, n-1)
 
print(' '.join(map(str, a)))
