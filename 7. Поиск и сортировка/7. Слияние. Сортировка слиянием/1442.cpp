#include <iostream>

using namespace std;

typedef long long i64;

i64 pow2(i64 i)
{
    return i*i;
}

i64 pow3(i64 i)
{
    return i*i*i;
}

int main()
{
    int x;
    cin >> x;

    i64 i = 1;
    i64 j = 1;

    i64 a;
    i64 b;

    for (int k = 0; k < x; k ++) {
        a = pow2(i);
        b = pow3(j);

        if (a < b) {
            i ++;
        } else if (a > b) {
            j ++;
        } else {
            i ++;
            j ++;
        }
    }

    cout << min(a, b);

    return 0;
}
