
n = int(input())
a = list(map(int, input().split()))

for i in range(n-1, 0, -1):
    maxi = i

    for j in range(i):
        if a[maxi] < a[j]: maxi = j

    a[i], a[maxi] = a[maxi], a[i]

print(' '.join(map(str, a)))
