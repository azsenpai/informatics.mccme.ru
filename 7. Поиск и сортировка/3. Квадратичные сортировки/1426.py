
n, m = map(int, input().split())

a = [0]*n

for i in range(n):
    a[i] = tuple(map(int, input().split()))

k = int(input())
b = list(map(int, input().split()))

u = [[False]*m for i in range(n)]

for i in range(k, 0, -1):
    for j in range(1, i):
        if b[j-1] > b[j]:
            b[j-1], b[j] = b[j], b[j-1]

c = 0

for h in range(k):
    mini = minj = None
    for i in range(n):
        for j in range(m):
            if not u[i][j] and b[h] <= a[i][j] and (mini == None or a[mini][minj] > a[i][j]):
                mini = i
                minj = j
    if mini != None:
        u[mini][minj] = True
        c += 1
    
print(c)
