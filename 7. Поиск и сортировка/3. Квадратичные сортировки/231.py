
n = int(input())
a = list(map(int, input().split()))

x, k = map(int, input().split())

a.append(0)
k -= 1

for i in range(n, k, -1):
    a[i] = a[i-1]

a[k] = x

print(' '.join(map(str, a)))
