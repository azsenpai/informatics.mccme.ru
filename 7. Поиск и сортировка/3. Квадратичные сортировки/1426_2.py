
n, m = map(int, input().split())
a = []

for i in range(n):
    a.extend(list(map(int, input().split())))

n *= m

k = int(input())
b = list(map(int, input().split()))

c = 0

a.sort()
b.sort()

j = 0

for i in range(k):
    while j < n and a[j] < b[i]:
        j += 1
    if j < n:
        c += 1
        j += 1

print(c)
