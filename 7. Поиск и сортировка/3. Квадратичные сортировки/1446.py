
n = int(input())
a = [0]*n

for i in range(n):
    a[i] = list(map(int, input().split()))

for i in range(n, 0, -1):
    for j in range(1, i):
        if a[j][1] > a[j-1][1] or (a[j][1] == a[j-1][1] and a[j][0] < a[j-1][0]):
            a[j], a[j-1] = a[j-1], a[j]

for i in range(n):
    print(a[i][0], a[i][1])
