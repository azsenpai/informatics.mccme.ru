
n = int(input())
a = list(map(int, input().split()))

maxi = None

for i in range(n):
    if maxi == None or a[maxi] < a[i]:
        maxi = i

a[0], a[maxi] = a[maxi], a[0]

print(' '.join(map(str, a)))
