
def f(k):
    return n <= (k // x + k // y)

def bsearch():
    l = 0
    r = n * x

    while l < r:
        m = (l + r) // 2
        if f(m): r = m
        else: l = m + 1

    return l

n, x, y = map(int, input().split())

if x > y: x, y = y, x

if n == 1:
    print(x)
else:
    n -= 1
    print(x + bsearch())
