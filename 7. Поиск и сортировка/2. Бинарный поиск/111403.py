
from math import sqrt

EPS = 0.00000001

def f(x):
    return x*x + sqrt(x) - c > EPS

def bsearch():
    l = 0
    r = sqrt(c)

    while r - l > EPS:
        m = (l + r) / 2
        if f(m): r = m
        else: l = m

    return l

c = float(input())

print("%.9f" % bsearch())
