
import bisect
from collections import deque

_buf = deque()

def next_int():
    try:
        while len(_buf) == 0:
            _buf.extend(input().split())
    except:
        pass

    if len(_buf) > 0: return int(_buf.popleft())

    return None

n = next_int()
k = next_int()

a = [0]*n

for i in range(n):
    a[i] = next_int()

for i in range(k):
    x = next_int()
    j = bisect.bisect_left(a, x)

    print('YES' if j < n and a[j] == x else 'NO')
