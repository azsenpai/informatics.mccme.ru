// $Id: written by: ZHUMAZHANOV ADLET.$
// $Id: 15.11.11, 21:53 $ 

#include <iostream>
#include <cstdio>
#include <cmath>

#define echo(x)     clog << #x << " = " << x << "\n"

using namespace std;

const double EPS = 1E-15;

double a;
int n;

void readInputData()
{
    cin >> a
        >> n;
}

bool f(double b)
{
    return pow(b, n) < a;
}

double bin_search(double lower, double upper)
{
    double middle;

    while (lower+EPS < upper) {
        middle = (lower + upper) / 2;
        if (a - pow(middle, n) > 1E-9) {
            lower = middle;
        } else {
            upper = middle;
        }
    }

    return lower;
}

int main()
{
    ios_base::sync_with_stdio(0);

    //freopen("input.txt", "r", stdin);
    //freopen("output.txt", "w", stdout);

    readInputData();

    cout.precision(15);
//    cout << fixed << bin_search(0, a);
    cout << fixed << pow(a, 1.0 / n);

    return 0;
}
