#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

const int MAX_N = 100000;

int n;
int r;
int c;

int h[MAX_N];

bool f(int maxd)
{
    int i = 0;
    int j = 0;
    int g = 0;

    while (j < n) {
        if (h[j] - h[i] <= maxd && j-i+1 == c) {
            g += 1;
            i = j + 1;
        } else {
            while (h[j] - h[i] > maxd) {
                i += 1;
            }
        }
        j += 1;
    }

    return g >= r;
}

int bsearch()
{
    int l = 0;
    int r = h[n-1];
    int m;

    while (l < r) {
        m = (l + r) / 2;
        if (f(m)) {
            r = m;
        } else {
            l = m + 1;
        }
    }

    return l;
}

int main()
{
    cin >> n >> r >> c;

    for (int i = 0; i < n; i ++) {
        cin >> h[i];
    }

    sort(h, h + n);

    cout << bsearch();

    return 0;
}
