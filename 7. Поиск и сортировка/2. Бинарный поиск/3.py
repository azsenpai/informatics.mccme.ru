
from math import log2

n = int(input())
m = int(log2(n))

print(m if 2 ** m == n else m + 1)
