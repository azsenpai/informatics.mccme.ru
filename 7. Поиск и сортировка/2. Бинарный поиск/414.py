
def f(k):
    c = 0
    i = 0

    for j in range(k):
        while i < m:
            if b[j][0] < a[i][0] < b[n-k+j][0]:
                c += 1
                i += 1
                break
            i += 1

    return c >= k

def bsearch():
    l = 0
    r = min(m, n // 2)

    while l < r:
        x = (l + r + 1) // 2
        if f(x): l = x
        else: r = x - 1

    return r

m, n = map(int, input().split())

a = list(map(int, input().split()))
b = list(map(int, input().split()))

for i in range(m): a[i] = (a[i], i + 1)
for i in range(n): b[i] = (b[i], i + 1)

a.sort()
b.sort()

k = bsearch()

print(k)

i = 0

for j in range(k):
    while i < m:
        if b[j][0] < a[i][0] < b[n-k+j][0]:
            print(a[i][1], b[j][1], b[n-k+j][1])
            i += 1
            break
        i += 1
