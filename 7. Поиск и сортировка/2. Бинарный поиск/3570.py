
EPS = 0.000000001

def f(x):
    return (x ** n) < a

def bsearch():
    l = 0
    r = a

    while r - l > EPS:
        m = (l + r) / 2
        if f(m): l = m
        else: r = m

    return l

a = float(input())
n = int(input())

if a < 1:
    a = 1 / a
    print(1 / bsearch())
elif a == 1:
    print(1)
else:
    print(bsearch())

#print(a ** (1 / n))
