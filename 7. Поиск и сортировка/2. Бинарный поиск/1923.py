
def f(k):
    return (k//w) * (k//h) >= n

def bsearch():
    l = 1
    r = max(n*w, n*h)

    while l < r:
        m = (l + r) // 2
        if f(m): r = m
        else: l = m + 1

    return l

w, h, n = map(int, input().split())
print(bsearch())
