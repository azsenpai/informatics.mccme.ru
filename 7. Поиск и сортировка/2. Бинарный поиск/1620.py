
def f(maxd):
    g = i = j = 0

    while j < n:
        if h[j] - h[i] <= maxd:
            if j-i+1 == c:
                g += 1
                i = j + 1
        else:
            while h[j] - h[i] > maxd:
                i += 1
        j += 1

    return g >= r

def bsearch():
    l = 0
    r = h[n-1]

    while l < r:
        m = (l + r) // 2
        if f(m): r = m
        else: l = m + 1

    return l

n, r, c = map(int, input().split())

h = [0]*n

for i in range(n):
    h[i] = int(input())

h.sort()

print(bsearch())
