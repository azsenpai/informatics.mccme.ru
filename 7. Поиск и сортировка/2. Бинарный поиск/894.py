
from math import ceil

EPS = 0.000001

def f(wait):
    timer = wait

    for i in range(n):
        timer += dx[i] / vmax
        if timer >= t[i]: timer += d

    return timer >= t[n-1]

def bsearch():
    l = 0
    r = t[n-1]

    while r - l > EPS:
        m = (l + r) / 2
        if f(m): r = m
        else: l = m

    return l

vmax, d = map(int, input().split())
n = int(input())

dx = [0]*n

s = 0
x = 0
y = 0

t = [0]*n

for i in range(n):
    dt1 = input().split()
    x = int(dt1[0])
    dx[i] = x - y
    s += 2*dx[i]
    y = x

    dt2 = dt1[1].split(':')

    dt2[0] = dt2[0].strip('0')
    dt2[1] = dt2[1].strip('0')

    if len(dt2[0]) == 0: dt2[0] = '0'
    if len(dt2[1]) == 0: dt2[1] = '0'

    t[i] = int(dt2[0])*60 + int(dt2[1])

m = int(ceil(bsearch() + s/vmax + d*n))

h = m // 60
m %= 60

print(h//10, h%10, ':', m//10, m%10, sep = '')
