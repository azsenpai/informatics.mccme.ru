#include <iostream>
#include <cmath>

using namespace std;

const double EPS = 1E-8;

double a;
int n;

bool f(double x)
{
    return a - pow(x, n) > EPS;
}

double bsearch()
{
    double l = 0;
    double r = a;
    double m;

    while (r - l > EPS) {
        m = (l + r) / 2;

        if (f(m)) {
            l = m;
        } else {
            r = m;
        }
    }

    return l;
}

int main()
{
    cin >> a >> n;

    if (a < 1) {
        a = 1 / a;
        cout << 1 / bsearch();
    } else {
        cout << bsearch();
    }

    return 0;
}
