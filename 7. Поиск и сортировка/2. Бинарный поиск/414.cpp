#include <iostream>
#include <algorithm>

using namespace std;

const int MAX_N = 100000;

int m;
int n;

int a[MAX_N];
int b[MAX_N];

bool f(int k)
{
    int c = 0;
    int i = 0;

    for (int j = 0; j < k; j ++) {
        while (i < m) {
            if (b[j][0] < a[i][0] && a[i][0] < b[n-k+j][0]) {
                c += 1;
                i += 1;
                break;
            }
            i += 1;
        }
    }

    return c >= k;
}

int bsearch()
{
    int l = 0;
    int r = min(m, n / 2);
    int x;

    while (l < r) {
        x = (l + r + 1) / 2;

        if (f(x)) {
            l = x;
        } else {
            r = x - 1;
        }
    }

    return l;
}

int cmp(int* a, int* b)
{
    return a[0] < b[0];
}

int main()
{
    cin >> m >> n;

    for (int i = 0; i < m; i ++) {
        a[i] = new int[2];
        cin >> a[i][0];
        a[i][1] = i + 1;
    }

    for (int i = 0; i < n; i ++) {
        b[i] = new int[2];
        cin >> b[i][0];
        b[i][1] = i + 1;
    }

    sort(a, a + m, cmp);
    sort(b, b + n, cmp);

    int k = bsearch();

    cout << k << endl;

    int i = 0;

    for (int j = 0; j < k; j ++) {
        while (i < m) {
            if (b[j][0] < a[i][0] && a[i][0] < b[n-k+j][0]) {
                cout << a[i][1] << " " << b[j][1] << " " << b[n-k+j][1] << endl;
                i += 1;
                break;
            }
            i += 1;
        }
    }

    return 0;
}
