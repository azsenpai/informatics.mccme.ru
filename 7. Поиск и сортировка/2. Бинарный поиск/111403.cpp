#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

const double EPS = 1E-9;

double c;

bool f(double x)
{
    return x*x + sqrt(x) - c > EPS;
}

double bsearch()
{
    double l = 0;
    double r = sqrt(c);
    double m;

    while (r - l > EPS) {
        m = (l + r) / 2;
        if (f(m)) {
            r = m;
        } else {
            l = m;
        }
    }

    return l;
}

int main()
{
    cin >> c;
    cout.precision(9);
    cout << fixed << bsearch();
}
