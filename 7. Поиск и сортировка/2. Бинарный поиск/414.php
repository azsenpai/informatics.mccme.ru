<?php

function int($var)
{
    return (int)$var;
}

function input()
{
    return fgets(STDIN);
}

function f($k)
{
    global $m, $n;
    global $a, $b;

    if ($k == 0) {
        return true;
    }

    $c = 0;
    $i = 0;

    for ($j = 0; $j < $k; $j ++) {
        for (; $i < $m; $i ++) {
            if ($b[$j][0] < $a[$i][0] && $a[$i][0] < $b[$n-$k+$j][0]) {
                $c += 1;
                $i += 1;
                break;
            }
        }
    }

    return $c >= $k;
}

function bsearch()
{
    global $m;
    global $n;

    $l = 0;
    $r = min($m, intval($n / 2));

    while ($l < $r) {
        $x = intval(($l + $r + 1) / 2);
        if (f($x)) {
            $l = $x;
        } else {
            $r = $x - 1;
        }
    }

    return $l;
}

list($m, $n) = explode(' ', input());

$m = int($m);
$n = int($n);

$a = explode(' ', input());
$b = explode(' ', input());

for ($i = 0; $i < $m; $i ++) {
    $a[$i] = array(int($a[$i]), $i + 1);
}

for ($i = 0; $i < $n; $i ++) {
    $b[$i] = array(int($b[$i]), $i + 1);
}

sort($a);
sort($b);

$k = bsearch();

print($k);
print(PHP_EOL);

$i = 0;

for ($j = 0; $j < $k; $j ++) {
    for (; $i < $m; $i ++) {
        if ($b[$j][0] < $a[$i][0] && $a[$i][0] < $b[$n-$k+$j][0]) {
            print($a[$i][1] . ' ' . $b[$j][1] . ' ' . $b[$n-$k+$j][1]);
            print(PHP_EOL);
            $i += 1;
            break;
        }
    }
}
