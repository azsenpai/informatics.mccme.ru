#include <iostream>
#include <cmath>

using namespace std;

const double EPS = 1E-6;
const int MAX_N = 1400;

double vmax;
double d;

int n;

double dx[MAX_N];
double t[MAX_N];

bool f(double wait)
{
    double timer = wait;

    for (int i = 0; i < n; i ++) {
        timer += dx[i] / vmax;
        if (timer >= t[i]) {
            timer += d;
        }
    }

    return timer >= t[n-1];
}

double bsearch()
{
    double l = 0;
    double r = t[n-1];
    double m;

    while (r - l > EPS) {
        m = (l + r) / 2;
        if (f(m)) {
            r = m;
        } else {
            l = m;
        }
    }

    return l;
}

int main()
{
    cin >> vmax >> d;
    cin >> n;

    double s = 0;
    double x;
    double y = 0;

    char c;

    int h, m;

    for (int i = 0; i < n; i ++) {
        cin >> x;
        dx[i] = x - y;
        s += 2*dx[i];
        y = x;

        cin >> h >> c >> m;

        t[i] = h*60 + m;
    }

    m = (int)ceil(bsearch() + s/vmax + d*n);

    h = m / 60;
    m %= 60;

    cout << h / 10 << h % 10 << ":" << m / 10 << m % 10;
}
