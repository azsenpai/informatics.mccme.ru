
def f(y):
    c = 1
    i = 0
    j = 1

    while j < n:
        if x[j] - x[i] >= y:
            i = j
            c += 1
        j += 1

    return c >= k

def bsearch():
    l = 0
    r = max(x) - min(x)

    while l < r:
        m = (l + r + 1) // 2
        if f(m): l = m
        else: r = m - 1

    return l

n, k = map(int, input().split())

x = tuple(map(int, input().split()))

print(bsearch())
