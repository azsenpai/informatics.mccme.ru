
n = int(input())
c = list(map(int, input().split()))

k = int(input())
p = tuple(map(int, input().split()))

for pj in p: c[pj-1] -= 1

for i in range(n):
    print('yes' if c[i] < 0 else 'no')
