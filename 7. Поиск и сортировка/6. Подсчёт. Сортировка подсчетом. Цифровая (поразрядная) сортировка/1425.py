
n = int(input())
a = list(map(int, input().split()))

minv = min(a)
maxv = max(a)

c = [0]*(maxv-minv+1)

for x in a: c[maxv-x] += 1

for i in range(maxv-minv, -1, -1):
    for j in range(c[i]): print(maxv-i, end = ' ')
