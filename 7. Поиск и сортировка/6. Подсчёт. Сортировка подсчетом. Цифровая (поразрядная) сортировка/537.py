# -*- coding: utf-8 -*-

n = int(input())

c = [0] * n
p = [0] * n

name = [''] * n

for i in range(n):
    m = int(input())

    if m == 0:
        p[i] = i
        name[i] = input()
        c[i] += 1
    else:
        p[i] = p[m-1]
        c[p[i]] += 1

    input()

maxi = 0

for i in range(1, n):
    if c[maxi] < c[i]: maxi = i

print(name[maxi])
