#include <iostream>

using namespace std;

const int MAX_N = 1000;

int n;

int c[MAX_N];
int p[MAX_N];

string name[MAX_N];

int main()
{
    cin >> n;

    string tmp;
    int m;

    for (int i = 0; i < n; i ++) {
        cin >> m;
        cin.ignore();

        if (m == 0) {
            getline(cin, name[i]);
            p[i] = i;
            c[i] ++;
        } else {
            p[i] = p[m-1];
            c[p[i]] ++;
        }

        getline(cin, tmp);
    }

    int maxi = 0;

    for (int i = 1; i < n; i ++) {
        if (c[maxi] < c[i]) {
            maxi = i;
        }
    }

    cout << name[maxi];

    return 0;
}
