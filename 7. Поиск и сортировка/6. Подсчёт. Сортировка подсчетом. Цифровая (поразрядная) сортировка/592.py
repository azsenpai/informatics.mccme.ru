
n = int(input())
s = input()

N = ord('Z') - ord('A') + 1

c = [0]*N

for ch in s: c[ord(ch) - ord('A')] += 1

j = None

for i in range(N):
    if c[i]%2 == 1:
        j = i
        break

for i in range(N):
    if c[i]%2 == 1: c[i] -= 1

t = ''
for i in range(N):
    if c[i] > 0:
        t += chr(ord('A') + i) * (c[i] // 2)
        c[i] //= 2

if j != None: t += chr(ord('A') + j)

for i in range(N-1, -1, -1):
    if c[i] > 0:
        t += chr(ord('A') + i) * c[i]
        c[i] //= 2

print(t)
