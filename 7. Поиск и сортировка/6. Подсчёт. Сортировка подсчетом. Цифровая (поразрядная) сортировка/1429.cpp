#include <iostream>

using namespace std;

const int MAX_L = 10000 + 1;

int L;
int n;
int m;

int c[MAX_L];

int main()
{
    cin >> L >> n >> m;

    for (int i = 0, l, r; i < n; i ++) {
        cin >> l >> r;
        for (int j = l; j <= r; j ++) {
            c[j] ++;
        }
    }

    for (int i = 0, x; i < m; i ++) {
        cin >> x;
        cout << c[x] << endl;
    }

    return 0;
}
