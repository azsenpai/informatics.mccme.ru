
L, n, m = map(int, input().split())

c = [0]*L

for i in range(n):
    l, r = map(int, input().split())
    for j in range(l-1, r): c[j] += 1

for i in range(m):
    x = int(input()) - 1
    print(c[x])
