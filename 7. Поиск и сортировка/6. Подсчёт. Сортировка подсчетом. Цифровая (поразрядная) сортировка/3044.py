
MAX_A = 10000

n = int(input())
a = list(map(int, input().split()))

c = [0] * (2*MAX_A + 1)

for x in a: c[x+MAX_A] += 1

for i in range(2*MAX_A + 1):
    for j in range(c[i]): print(i - MAX_A, end = ' ')
