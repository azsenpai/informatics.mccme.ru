
s = input()
t = input()

N = ord('9') - ord('0') + 1 + ord('z') - ord('a') + 1

c1 = [0] * N
c2 = [0] * N

for c in s:
    if 'a' <= c <= 'z':
        c1[ord(c)-ord('a')+10] += 1
    elif '0' <= c <= '9':
        c1[ord(c)-ord('0')] += 1

for c in t:
    if 'a' <= c <= 'z':
        c2[ord(c)-ord('a')+10] += 1
    elif '0' <= c <= '9':
        c2[ord(c)-ord('0')] += 1

flag = True

for i in range(N):
    if c1[i] != c2[i]:
        flag = False
        break

print('YES' if flag else 'NO')
