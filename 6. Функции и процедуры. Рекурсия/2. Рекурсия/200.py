
dx = (-1,  0, 0, 1)
dy = ( 0, -1, 1, 0)

def walk(i, j):
    if a[i][j] != '.': return
    a[i][j] = 'x'

    for k in range(4): walk(i+dx[k], j+dy[k])

n = int(input())
a = ['.'] * n

for i in range(n):
    a[i] = list(input())

i, j = map(int, input().split())
walk(i-1, j-1)

c = 0
for i in range(n):
    c += a[i].count('x')

print(c)
