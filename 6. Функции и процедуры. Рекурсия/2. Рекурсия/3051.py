
def hanoi(n, a, b, c):
    if n == 1:
        if (a == 1 and c == 3) or (a == 3 and c == 1):
            print(n, a, b)
            print(n, b, c)
        else:
            print(n, a, c)
    else:
        hanoi(n-1, a, b, c)
        print(n, a, b)
        hanoi(n-1, c, b, a)
        print(n, b, c)
        hanoi(n-1, a, b, c)

n = int(input())

hanoi(n, 1, 2, 3)
