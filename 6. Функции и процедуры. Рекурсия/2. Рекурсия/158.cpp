#include <iostream>

using namespace std;

int n;
int a[8];

int sqrt3(int n)
{
    int l = 1;
    int r = 1260;
    int m;

    while (l < r) {
        m = (l + r + 1) >> 1;

        if (m*m*m > n) {
            r = m - 1;
        } else {
            l = m;
        }
    }

    return l;
}

void rec(int i, int s)
{
    if (i > 8 || (i == 8 && s != 0)) {
        return;
    }

    if (s == 0) {
        for (int j = 0; j < i; j ++) {
            cout << a[j] << " ";
        }
        exit(0);
    }

    for (int j = sqrt3(s); j >= 1; j --) {
        a[i] = j;
        rec(i+1, s - j*j*j);
    }
}

int main()
{
    cin >> n;

    rec(0, n);

    cout << "IMPOSSIBLE";

    return 0;
}
