
def print_step(n, a, b, c):
    if (a == 1 and c == 2) or (a == 2 and c == 3) or (a == 3 and c == 1):
        print(n, a, c)
    else:
        print(n, a, b)
        print(n, b, c)

def hanoi(n, a, b, c):
    if n == 1:
        print_step(n, a, b, c)
    else:
        hanoi(n-1, a, b, c)

        print('else1')
        print_step(n, a, c, b)

        hanoi(n-1, c, b, a)
 
        print('else2')
        print_step(n, b, a, c)
 
        hanoi(n-1, a, b, c)
 
n = int(input())
 
hanoi(n, 1, 2, 3)
