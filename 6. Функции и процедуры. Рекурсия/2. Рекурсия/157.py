
def rec(i, s, c):
    if i >= m:
        global res_c
        global res_m

        if s == 0 and (res_c == None or res_c > c):
            res_c = c
            res_m = mask[:]

        return

    mask[i] = 0
    rec(i + 1, s, c)

    sa = a[i]

    if (s - sa >= 0):
        mask[i] = 1
        rec(i + 1, s - sa, c + 1)

    sa = sa + a[i]

    if (s - sa >= 0):
        mask[i] = 2
        rec(i + 1, s - sa, c + 2)

def no_rec():
    global res_c, res_m
    global mask

    s = c = 0

    while True:
        i = 0

        while i < m:
            mask[i] += 1

            if mask[i] == 3:
                mask[i] = 0

                s -= 2*a[i]
                c -= 2

                i += 1 
            else:
                s += a[i]
                c += 1

                break

        if s == n and (res_c == None or res_c > c):
            res_c = c
            res_m = mask[:]

        if i >= m: break
        

n, m = map(int, input().split())

a = list(map(int, input().split()))
mask = [0] * m

res_c = None
res_m = None

if 2*sum(a) < n:
    print(-1)
else:
    #rec(0, n, 0)
    no_rec()

    if res_c == None:
        print(0)
    else:
        print(res_c)

        for i in range(m):
            for j in range(res_m[i]): print(a[i], end = ' ')
