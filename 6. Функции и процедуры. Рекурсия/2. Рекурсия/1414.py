
def rem(n):
    if n == 1:
        print(-n, end = ' ')
        return
    put(n-1)
    print(-n, end = ' ')
    rem(n-1)

def put(n):
    if n == 1:
        print(n, end = ' ')
        return
    put(n-1)
    print(n, end = ' ')
    rem(n-1)

n = int(input())

for i in range(n, 0, -1):
    put(i)
