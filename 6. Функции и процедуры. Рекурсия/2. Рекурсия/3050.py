
def hanoi(n, a, b, c):
    if n == 1:
        print(n, a, c)
        return
    else:
        hanoi(n-1, a, c, b)
        print(n, a, c)
        hanoi(n-1, b, a, c)

n = int(input())

hanoi(n, 1, 2, 3)
