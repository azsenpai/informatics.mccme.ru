#include <iostream>
#include <cstring>

using namespace std;

typedef long long i64;

const int MAX_M = 15;

int n;
int m;

int a[MAX_M];
int b[MAX_M];

int k = -1;
int c[MAX_M];

void rec(int i, int s, int cnt)
{
    if (i == m) {
        if (s == 0 && (k == -1 || k > cnt)) {
            k = cnt;
            memcpy(c, b, m*sizeof(int));
        }

        return;
    }
    for (int j = 0; j < 3; j ++) {
        b[i] = j;
        rec(i+1, s - a[i]*b[i], cnt + b[i]);
    }
}

int main()
{
    cin >> n >> m;

    i64 sum = 0;

    for (int i = 0; i < m; i ++) {
        cin >> a[i];
        sum += 2 * a[i];
    }

    if (sum < n) {
        cout << -1;
    } else {
        rec(0, n, 0);
        if (k == -1) {
            cout << 0;
        } else {
            cout << k << endl;
            for (int i = 0; i < m; i ++) {
                for (int j = 0; j < c[i]; j ++) {
                    cout << a[i] << " ";
                }
            }
        }
    }

    return 0;
}
