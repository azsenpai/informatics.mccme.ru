
def rec(n, a):
    global minc

    if n == 0: return
    if n == 1:
        if minc == None or (abs(c - a[0]) < abs(c - minc)):
            minc = a[0]
        return

    for i in range(n):
        for j in range(i+1, n):
            l = [a[i] + a[j]]
            for k in range(n):
                if k != i and k != j: l.append(a[k])
            rec(n - 1, l)

            l = [(a[i] * a[j]) / (a[i] + a[j])]
            for k in range(n):
                if k != i and k != j: l.append(a[k])
            rec(n - 1, l)

n, c = input().split()

n = int(n)
c = float(c)

a = list(map(float, input().split()))

minc = None

rec(n, a)

print(minc)
