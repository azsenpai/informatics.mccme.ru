
import timeit

def sqrt3(n):
    l = 1
    r = 1260

    while l < r:
        m = (l + r + 1) >> 1
        if m*m*m > n:
            r = m-1
        else:
            l = m

    return l

def rec(i, s):
    global a, start

    if i > 8 or (i == 8 and s != 0): return

    if s == 0:
        for j in range(i): print(a[j], end = ' ')

        stop = timeit.default_timer()
        #print()
        #print(stop - start)

        exit()

    j = sqrt3(s)

    while j >= 1:
        a[i] = j
        rec(i+1, s - j*j*j)
        j -= 1
        

    #for j in range(sqrt3(s), 0, -1):
    #    a[i] = j
    #    rec(i+1, s - j*j*j)

n = int(input())
a = [0]*8

start = timeit.default_timer()

rec(0, n)

print('IMPOSSIBLE')

stop = timeit.default_timer()
#print()
#print(stop - start)
