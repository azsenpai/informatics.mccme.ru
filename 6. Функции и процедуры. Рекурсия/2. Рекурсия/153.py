
def phi(n):
    if n == 0: return 0
    if n == 1: return 1

    return phi(n-1) + phi(n-2)

n = int(input())
print(phi(n))
