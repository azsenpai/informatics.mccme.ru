
from collections import deque

buffer = deque()

def next_int():
    global buffer

    try:
        while len(buffer) == 0:
            buffer.extend(input().split())
    except:
        pass

    if len(buffer) > 0: return int(buffer.popleft())

    return None

def rec(i):
    if i == n: return
    a = next_int()
    rec(i+1)
    print(a, end = ' ')

n = int(input())
rec(0)
