
dx = (0, 1,  0, -1)
dy = (1, 0, -1,  0)

def fill(i, j, k, d):
    if k > n*n or not (0 <= i < n) or not (0 <= j < n) or a[i][j] > 0: return
    a[i][j] = k

    fill(i+dx[d], j+dy[d], k+1, d)

    d = (d + 1) % 4
    fill(i+dx[d], j+dy[d], k+1, d)

n = int(input())
a = [[0] * n for i in range(n)]

fill(0, 0, 1, 0)

for i in range(n):
    print(' '.join(map(str, a[i])))
