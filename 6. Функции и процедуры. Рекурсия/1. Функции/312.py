
def phi(n):
    f0 = 1
    f1 = 1

    if n == 0: return f0
    if n == 1: return f1

    f2 = 0

    for i in range(2, n + 1):
        f2 = f1 + f0
        f0, f1 = f1, f2

    return f2

n = int(input())
print(phi(n))
