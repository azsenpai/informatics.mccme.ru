
n = int(input())

r = n % 10
t = n % 100

if r == 0:
    print(n, 'bochek')
elif r == 1:
    if t == 11:
        print(n, 'bochek')
    else:
        print(n, 'bochka')
elif r == 2 or r == 3 or r == 4:
    if t == 12 or t == 13 or t == 14:
        print(n, 'bochek')
    else:
        print(n, 'bochki')
else:
    print(n, 'bochek')
