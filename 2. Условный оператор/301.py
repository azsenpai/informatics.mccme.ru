
import math

a = float(input())
b = float(input())
c = float(input())

D = b*b - 4*a*c

if D > 0:
    print((-b + math.sqrt(D)) / (2 * a))
    print((-b - math.sqrt(D)) / (2 * a))
elif D == 0:
    print(-b / (2 * a))
