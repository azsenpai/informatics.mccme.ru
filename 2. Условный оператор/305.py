
n = int(input())

t1  = 0
t5  = 0
t10 = 0
t20 = 0
t60 = 0

t60 = n // 60
n %= 60

if n > 35:
    t60 += 1
else:
    t20 = n // 20
    n %= 20

    if n > 17:
        t20 += 1
    else:
        t10 = n // 10
        n %= 10

        if n > 8:
            t10 += 1
        else:
            t5 = n // 5
            t1 = n % 5

print(t1, t5, t10, t20, t60)
