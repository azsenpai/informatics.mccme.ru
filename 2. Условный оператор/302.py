
import math

a = int(input())
b = int(input())
c = int(input())

if a > b:
    a, b = b, a

if a > c:
    a, c = c, a

if b > c:
    b, c = c, b

if a + b <= c:
    print('impossible')
elif a*a + b*b == c*c:
    print('right')
elif a*a + b*b - c*c > 0:
    print('acute')
else:
    print('obtuse')
