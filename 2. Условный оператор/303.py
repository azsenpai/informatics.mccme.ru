
n = int(input())

r = n % 10

if r == 0:
    print(n, 'korov')
elif r == 1:
    if n == 11:
        print(n, 'korov')
    else:
        print(n, 'korova')
elif r == 2 or r == 3 or r == 4:
    if n == 12 or n == 13 or n == 14:
        print(n, 'korov')
    else:
        print(n, 'korovy')
else:
    print(n, 'korov')
