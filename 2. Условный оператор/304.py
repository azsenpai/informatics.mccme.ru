
n = int(input())

t1  = 0
t10 = 0
t60 = 0

t60 = n // 60
n %= 60

if n > 34:
    t60 += 1
else:
    t10 = n // 10
    n %= 10

    if n > 8:
        t10 += 1
    else:
        t1 = n

print(t1, t10, t60)
