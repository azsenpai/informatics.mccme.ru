
x = int(input())
r = ''

if x == 100:
    r += 'C'
else:
    t = x // 10
    n = x % 10

    if t == 1:
        r += 'X'
    elif t == 2:
        r += 'XX'
    elif t == 3:
        r += 'XXX'
    elif t == 4:
        r += 'XL'
    elif t == 5:
        r += 'L'
    elif t == 6:
        r += 'LX'
    elif t == 7:
        r += 'LXX'
    elif t == 8:
        r += 'LXXX'
    elif t == 9:
        r += 'XC'

    if n == 1:
        r += 'I'
    elif n == 2:
        r += 'II'
    elif n == 3:
        r += 'III'
    elif n == 4:
        r += 'IV'
    elif n == 5:
        r += 'V'
    elif n == 6:
        r += 'VI'
    elif n == 7:
        r += 'VII'
    elif n == 8:
        r += 'VIII'
    elif n == 9:
        r += 'IX'

print(r)
