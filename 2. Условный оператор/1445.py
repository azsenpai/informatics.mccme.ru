
m, n = (int(item) for item in input().split())
x, y = (int(item) for item in input().split())

if 1 <= x + 1 <= m:
    print(x + 1, y)

if 1 <= x - 1 <= m:
    print(x - 1, y)

if 1 <= y + 1 <= n:
    print(x, y + 1)

if 1 <= y - 1 <= n:
    print(x, y - 1)
