
s = list(input())
i, j = map(int, input().split())

i -= 1
j -= 1

while i < j:
    s[i], s[j] = s[j], s[i]
    i += 1
    j -= 1

print(''.join(s))
