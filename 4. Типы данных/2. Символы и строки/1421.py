
s = input()
t = ''

add_space = True

for c in s:
    if c == ' ':
        if add_space:
            t += c
            add_space = False
    else:
        t += c
        add_space = True
            
print(t)
