
s = input()

print(s[2])     #1
print(s[-2])    #2
print(s[:5])    #3
print(s[:-2])   #4

t = ''
for i in range(0, len(s), 2):
    t += s[i]

print(t)        #6

t = ''
for i in range(1, len(s), 2):
    t += s[i]

print(t)        #5

print(s[::-1])  #7
print(s[::-2])  #8

print(len(s))   #9
