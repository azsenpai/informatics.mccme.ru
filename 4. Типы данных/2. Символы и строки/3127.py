
s = input()
ch = 'h'
c = s.count(ch)

if c > 2:
    s = s.replace(ch, 'H', c - 1)
    s = s.replace('H', ch, 1)

print(s)
