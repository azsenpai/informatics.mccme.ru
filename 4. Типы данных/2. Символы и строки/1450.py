
s = input()
t = ''

for l in range(len(s), 0, -1):
    for i in range(len(s) - l + 1):
        t = s[i:i+l]
        if t == t[::-1]: break
        else: t = ''
    if len(t) > 0: break

print(t)
