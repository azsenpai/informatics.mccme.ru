
s = input()
k = int(input())

l = ord('A')
r = ord('Z')

t = ''

for c in s:
    if ord(c) - k < l:
        t += chr(r - l + ord(c) - k + 1)
    else:
        t += chr(ord(c) - k)

print(t)
