
import sys

x = 0
y = 0

for line in sys.stdin:
    cmd = line.split()
    if cmd[0] == 'North':
        y += int(cmd[1])
    elif cmd[0] == 'South':
        y -= int(cmd[1])
    elif cmd[0] == 'East':
        x += int(cmd[1])
    elif cmd[0] == 'West':
        x -= int(cmd[1])

print(x, y)
