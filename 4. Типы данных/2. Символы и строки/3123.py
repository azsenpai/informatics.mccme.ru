
s = input()

ch = 'h'
c = s.count(ch)

if c > 1:
    i = s.find(ch)
    j = s.rfind(ch)

    print(s[:i], s[j:i-1 if i > 0 else None:-1], s[j+1:], sep = '')
