
def is_ip(s):
    try:
        ip = list(map(int, s.split('.')))
    except:
        return False

    if len(ip) != 4: return False

    l = 0
    r = 255

    for n in ip:
        if not (l <= n <= r):
            return False

    return True

print(1 if is_ip(input()) else 0)
