
s = input()
t = ''

n = None

for c in s:
    if '0' <= c <= '9':
        n = int(c) if n == None else n*10 + int(c)
    else:
        if n != None: t += bin(n)[2:]
        n = None
        t += c

if n != None: t += bin(n)[2:]

print(t)

