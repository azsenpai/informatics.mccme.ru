
x, n = map(int, input().split())

a = str(x)
b = ''


for i in range(n):
    b = ''
    n = 0
    for j in range(len(a)):
        if j == 0 or a[j-1] == a[j]:
            n += 1
        else:
            b += str(n) + a[j-1]
            n = 1
    b += str(n) + a[j]
    a, b = b, a

print(b)
