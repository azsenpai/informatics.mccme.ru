
n = int(input())

a = []
b = []
c = []

for i in range(n):
    s = input().split()

    a.append(int(s[2]))
    b.append(int(s[3]))
    c.append(int(s[4]))

print(sum(a) / n, sum(b) / n, sum(c) / n)
