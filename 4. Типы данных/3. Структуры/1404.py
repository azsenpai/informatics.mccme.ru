
class Student:
    def __init__(self, name, surename, index, dob):
        self.name = name
        self.surename = surename
        self.index = index
        self.dob = dob

        self.sort_weight = index
        if len(self.sort_weight) < 3:
            self.sort_weight = '0' + self.sort_weight

        self.sort_weight += surename

    def __lt__(self, right):
        return self.sort_weight < right.sort_weight

n = int(input())
s = []

for i in range(n):
    surename = input()
    name = input()
    index = input()
    dob = input()
    s.append(Student(name, surename, index, dob))

s.sort()

for i in range(n):
    print(s[i].index, s[i].surename, s[i].name, s[i].dob)

