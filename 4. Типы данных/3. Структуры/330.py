
class Student:
    def __init__(self, s):
        l = s.split()

        self.name = l[0]
        self.surename = l[1]

        self.p = []

        self.p.append(int(l[2]))
        self.p.append(int(l[3]))
        self.p.append(int(l[4]))

    def getp(self):
        return (self.p[0] + self.p[1] + self.p[2]) / 3


n = int(input())
s = []
max_p = 0

for i in range(n):
    s.append(Student(input()))
    p = s[i].getp()
    if max_p < p: max_p = p

for i in range(n):
    if max_p == s[i].getp():
        print(s[i].name, s[i].surename)
