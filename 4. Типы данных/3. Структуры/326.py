
import math

class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def dist(self, right):
        return math.sqrt((self.x - right.x)*(self.x - right.x) + (self.y - right.y)*(self.y - right.y))

n = int(input())
p = []

for i in range(n):
    x, y = map(int, input().split())
    p.append(Point(x, y))

max_d = 0

for i in range(n):
    for j in range(i + 1, n):
        a = p[i].dist(p[j])
        for k in range(j + 1, n):
            d = a + p[j].dist(p[k]) + p[k].dist(p[i])
            if max_d < d: max_d = d

print(max_d)
