
class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def dist2(self, x = 0, y = 0):
        return (self.x - x)*(self.x - x) + (self.y - y)*(self.y - y)

n = int(input())
a = Point()

for i in range(n):
    x, y = map(int, input().split())
    b = Point(x, y)

    if a.dist2() < b.dist2(): a = b

print(a.x, a.y)
