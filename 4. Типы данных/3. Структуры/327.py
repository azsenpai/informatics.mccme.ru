
import math

class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def dist(self, right):
        return math.sqrt((self.x - right.x)*(self.x - right.x) + (self.y - right.y)*(self.y - right.y))

class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def is_triangle(self):
        return self.a + self.b > self.c     \
            and self.b + self.c > self.a    \
            and self.c + self.a > self.b

    def gets2(self):
        p = (self.a + self.b + self.c) / 2
        return p*(p - self.a)*(p - self.b)*(p - self.c)

def gets2(a, b, c):
    p = (a + b + c) / 2
    return p*(p - a)*(p - b)*(p - c)
    

n = int(input())
p = []
d = [0] * n

for i in range(n):
    x, y = map(int, input().split())
    p.append(Point(x, y))
    d[i] = [0] * n

for i in range(n):
    for j in range(i + 1, n):
        d[i][j] = d[j][i] = p[i].dist(p[j])

max_s = 0

for i in range(n):  
    for j in range(i + 1, n):
        for k in range(j + 1, n):
            #t = Triangle(d[i][j], d[j][k], d[k][i])
            #if t.is_triangle():
            s = gets2(d[i][j], d[j][k], d[k][i])
            if max_s < s: max_s = s

print(math.sqrt(max_s))
