
class Student:
    def __init__(self, s):
        l = s.split()

        self.name = l[0]
        self.surename = l[1]

        self.p = []

        self.p.append(int(l[2]))
        self.p.append(int(l[3]))
        self.p.append(int(l[4]))

    def getp(self):
        return (self.p[0] + self.p[1] + self.p[2]) / 3


n = int(input())
s = []

max_p1 = 0
max_p2 = 0
max_p3 = 0

for i in range(n):
    s.append(Student(input()))
    p = s[i].getp()

    if max_p1 <= p:
        max_p1, max_p2, max_p3 = p, max_p1, max_p2
    elif max_p2 <= p:
        max_p2, max_p3 = p, max_p2
    elif max_p3 <= p:
        max_p3 = p

for i in range(n):
    if s[i].getp() >= max_p3:
        print(s[i].name, s[i].surename)
