
class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def dist2(self, right):
        return (self.x - right.x)*(self.x - right.x) + (self.y - right.y)*(self.y - right.y)

n = int(input())
p = []

for i in range(n):
    x, y = map(int, input().split())
    p.append(Point(x, y))

o = Point()

for i in range(n):
    for j in range(i+1, n):
        if p[i].dist2(o) > p[j].dist2(o): p[i], p[j] = p[j], p[i]

for i in range(n):
    print(p[i].x, p[i].y)
