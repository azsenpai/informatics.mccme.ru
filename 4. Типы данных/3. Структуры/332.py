
class Student:
    def __init__(self, s):
        l = s.split()

        self.name = l[0]
        self.surename = l[1]

        self.p = (int(l[2]) + int(l[3]) + int(l[4])) / 3


n = int(input())
s = []

for i in range(n):
    s.append(Student(input()))
    #s[i] = Student(input())

s.sort(key = lambda student: student.p, reverse = True)

for i in range(n):
    print(s[i].name, s[i].surename)
