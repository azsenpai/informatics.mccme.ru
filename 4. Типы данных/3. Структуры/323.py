
class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __add__(self, right):
        return Point(self.x + right.x, self.y + right.y)

n = int(input())
a = Point()

for i in range(n):
    x, y = map(int, input().split())
    b = Point(x, y)

    a = a + b

print(a.x / n, a.y / n)
