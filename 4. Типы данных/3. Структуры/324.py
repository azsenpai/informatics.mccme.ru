
import math

class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def dist2(self, right):
        return (self.x - right.x)*(self.x - right.x) + (self.y - right.y)*(self.y - right.y)

n = int(input())
p = []

for i in range(n):
    x, y = map(int, input().split())
    p.append(Point(x, y))

max_d = 0

for i in range(n):
    for j in range(i + 1, n):
        d = p[i].dist2(p[j])
        if max_d < d: max_d = d

print(math.sqrt(max_d))
