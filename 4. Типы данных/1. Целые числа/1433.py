
n, m = (int(i) for i in input().split())
print((m + n - 1) // n)
