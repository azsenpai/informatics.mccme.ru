
n = int(input())

if n > 0:
    print(((1 + n) * n) // 2)
else:
    n = abs(n)
    print(-((1 + n) * n) // 2 + 1)
