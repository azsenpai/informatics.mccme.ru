
k, n = map(int, input().split())
r = n % k

print((n + k - 1) // k, r if r > 0 else k)
