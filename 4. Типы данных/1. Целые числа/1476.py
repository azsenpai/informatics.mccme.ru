
d = int(input())

h = d // 30
m = (d % 30) * 2

print('It is %d hours %d minutes.' % (h, m))
