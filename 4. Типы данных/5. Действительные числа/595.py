
EPS = 0.00001

x, y, z = map(float, input().split())

n = int(input())

for i in range(n):
    a, b, c, q = map(float, input().split())
    x -= a * q
    y -= b * q
    z -= c * q

print('YES' if x < EPS and y < EPS and z < EPS else 'NO')
