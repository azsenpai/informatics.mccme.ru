
q = []
q_i = 0

def next_float():
    global q, q_i

    while q_i >= len(q):
        q += input().split()

    if q_i < len(q):
        q_i += 1
        return float(q[q_i - 1])

    return 0

x = next_float()
y = next_float()

EPS = 0.00000001

s = x
n = 1

while abs(y - s) > EPS:
    x += x * 0.7
    s += x
    n += 1

print(n)
