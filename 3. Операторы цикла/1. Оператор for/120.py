
n = int(input())
f = 1
s = 1

if n < 17:
    for i in range(1, n + 1):
        f *= i
        s += 1 / f
else:
    s = 2.7182818284590455

print(s)
