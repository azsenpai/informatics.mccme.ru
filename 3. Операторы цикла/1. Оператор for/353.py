
n = int(input())
s = 0

a = 2
p = 1

for i in range(n + 1):
    s += p
    p *= a

print(s)
