
n = int(input())
a = 0
b = 0
c = 0

for i in range(n):
    x = int(input())
    a += x == 0
    b += x > 0
    c += x < 0

print(a, b, c)
