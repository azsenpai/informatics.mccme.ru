
n = int(input())
s = 0

for i in range(n + 1):
    sign = 1
    if i%2 == 1:
        sign = -1
    s += sign * 1 / (2*i + 1)

print(4 * s)
