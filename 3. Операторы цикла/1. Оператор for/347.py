
n = int(input())
f = False

for i in range(n): f = f or int(input()) == 0

if f:
    print('YES')
else:
    print('NO')
