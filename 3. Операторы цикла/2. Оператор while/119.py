
k = int(input())
s = 0

for i in range(1, k + 1):
    n = i
    m = 0
    t = i
    while n > 0:
        m = m * 10 + n % 10
        n //= 10
    s += m == t

print(s)
