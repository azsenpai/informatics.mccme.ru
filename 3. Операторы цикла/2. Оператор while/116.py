
n = int(input())

mind = n % 10
maxd = n % 10

while n > 0:
    d = n % 10
    mind = min(mind, d)
    maxd = max(maxd, d)

    n //= 10

print(mind, maxd)
