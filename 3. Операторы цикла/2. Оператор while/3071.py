
x = int(input())

m1 = None
m2 = None

while x != 0:
    if m1 == None:
        m1 = x
    elif m1 > x:
        if m2 == None:
            m2 = x
        else:
            m2 = max(m2, x)
    else:
        m1, m2 = x, m1

    x = int(input())

print(m2)
