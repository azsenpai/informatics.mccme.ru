
a = 0
b = int(input())

if b != 0:
    a, b = b, int(input())

m1 = 0
m2 = 0

n1 = 1
n2 = 1

while b != 0:
    if a < b:
        n1 += 1
        m2 = max(m2, n2)
        n2 = 1
    elif a > b:
        n2 += 1
        m1 = max(m1, n1)
        n1 = 1
    else:
        m1 = max(m1, n1)
        n1 = 1

        m2 = max(m2, n2)
        n2 = 1

    a, b = b, int(input())

m1 = max(m1, n1)
m2 = max(m2, n2)

print(max(m1, m2))
