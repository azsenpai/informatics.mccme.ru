
a = int(input())
b = int(input())
n = int(input())

ca = 'A'
cb = 'B'

if a > b:
    a, b = b, a
    ca, cb = cb, ca

v = 0
impossible = True

for i in range(100000):
    if v + a <= b:
        v += a
    else:
        v += a - b
    if v == n:
        impossible = False
        break
    
if impossible:
    print('Impossible')
else:
    v = 0
    for i in range(100000):
        print('>', ca, sep = '')
        print(ca, '>', cb, sep = '')
        if v + a <= b:
            v += a
        else:
            print(cb, '>', sep = '')
            print(ca, '>', cb, sep = '')
            v += a - b
        if v == n:
            break
    