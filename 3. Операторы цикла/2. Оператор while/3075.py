
a = int(input())

f0 = 0
f1 = 1

f2 = 0
n = 1

while f2 < a:
    f2 = f1 + f0
    f0, f1 = f1, f2
    n += 1

if f2 == a:
    print(n)
else:
    print(-1)
