
n = int(input())
s = 0

while n > 0:
    s += n % 10 == 0
    n //= 10

print(s)
