
x = int(input())
m = None

while x != 0:
    if m == None:
        m = x
    else:
        m = max(m, x)
    x = int(input())

print(m)
