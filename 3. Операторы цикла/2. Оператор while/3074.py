
n = int(input())

f0 = 0
f1 = 1
f2 = 0

for i in range(1, n + 1):
    f2 = f1 + f0
    f0, f1 = f1, f2

print(f2)
