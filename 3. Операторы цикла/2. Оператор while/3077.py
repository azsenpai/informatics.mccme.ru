
a = 0
b = int(input())

m = 0
n = 1

while b != 0:
    if a == b:
        n += 1
    else:
        m = max(m, n)
        n = 1
    a, b = b, int(input())

m = max(m, n)

print(m)
