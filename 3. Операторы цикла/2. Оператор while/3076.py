
a = int(input())
b = int(input())

while a > b:
    if a % 2 == 1 or a // 2 < b:
        print(-1)
        a -= 1
    else:
        print(':2')
        a //= 2
