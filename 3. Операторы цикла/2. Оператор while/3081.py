
import math

x = int(input())
n = 0

s1 = 0
s2 = 0

while x != 0:
    s1 += x
    s2 += x * x
    n += 1
    x = int(input())

if n > 1:
    print(math.sqrt((s2 - s1*s1 / n) / (n - 1)))
