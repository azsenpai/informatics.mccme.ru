
a = 0
b = 0
c = int(input())

if c != 0:
    a, b, c = b, c, int(input())

if c != 0:
    a, b, c = b, c, int(input())

m = None
i = None
j = 3

while c != 0:
    if a < b > c:
        if i == None:
            i = j - 1
        elif m == None:
            m = j - 1 - i
            i = j - 1
        else:
            m = min(m, j - 1 - i)
            i = j - 1

    a, b, c = b, c, int(input())
    j += 1

if m == None: m = 0

print(m)
