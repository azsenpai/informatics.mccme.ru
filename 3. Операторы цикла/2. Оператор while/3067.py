
x = int(input())
n = 0

while x != 0:
    n += x % 2 == 0
    x = int(input())

print(n)
