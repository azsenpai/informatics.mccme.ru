
x = int(input())

m = None
n = 0

while x != 0:
    if m == None or m < x:
        m = x
        n = 1
    elif m == x:
        n += 1
        
    x = int(input())

print(n)
